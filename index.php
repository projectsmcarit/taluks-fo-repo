<?php
session_start();
global $error;
// connection file included
//Replacing with include $conn=mysqli_connect("localhost","root","","ktym_taluk");
include("connection.php");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form

    $myusername = mysqli_real_escape_string($conn, $_POST['username']);
    $mypassword = mysqli_real_escape_string($conn, $_POST['password']);

    $sql = "SELECT taluk_id FROM login WHERE username = '$myusername' and password = '$mypassword'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);
    $num_row = mysqli_num_rows($result);

    // If result matched $myusername and $mypassword, table row must be 1 row

    if ($num_row == 1) {
        //session_register("myusername");
        $_SESSION['login_user'] = $myusername;
        $_SESSION['taluk_id'] = $row['taluk_id'];

        header("location: Admin_Home.php");
    } else {
        $error = "Invalid Username or Password";
    }
}
?>
<html>
<head>
    <title>Login</title>
    <script type="text/javascript">
        function preventForward() {
            window.history.forward();
        }
        setTimeout("preventBack()", 5 * 60 * 1000);
        window.onunload = function() {
            null
        };
    </script>
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">-->
    <style>
        @import url("https://fonts.googleapis.com/icon?family=Material+Icons");
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap');

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Poppins', sans-serif
        }
        .material-icons {
            font-size: 18px;
            font-weight: 600;
            vertical-align: -15%
        }
        body {
            background: #ecf0f3
        }
        button,input {
            border: 0;
            outline: 0;
            font-size: 16px;
            border-radius: 16px;
            padding: 16px;
            background-color: #EBECF0;
            text-shadow: 1px 1px 0 #FFF;
        }
        label {
            display: block;
            margin: 20px 0;
            width: 100%;
        }
        input {
            margin-right: 8px;
            box-shadow: inset 5px 5px 5px #BABECC, inset -5px -5px 5px #FFF;
            width: 100%;
            font-weight: 500;
            box-sizing: border-box;
            transition: all 0.2s ease-in-out;
            appearance: none;
            -webkit-appearance: none;
        }
        input:focus {
            box-shadow: inset 1px 1px 2px #BABECC, inset -1px -1px 2px #FFF;
        }
        button {
            color: #61677C;
            font-weight: bold;
            box-shadow: -5px -5px 5px #FFF, 5px 5px 5px #BABECC;
            transition: all 0.2s ease-in-out;
            cursor: pointer;
            font-weight: 600;
        }
        button:active {
            box-shadow: inset 1px 1px 2px #BABECC, inset -1px -1px 2px #FFF;
        }
        button.blue {
            display: block;
            width: 100%;
            color: #03A9F4;
        }
        button:hover {
            box-shadow: -2px -2px 5px #FFF, 2px 2px 5px #BABECC;
        }
        .wrapper {
            max-width: 350px;
            min-height: 500px;
            margin: 80px auto;
            padding: 40px 30px 30px 30px;
            background-color: #ecf0f3;
            border-radius: 15px;
            box-shadow: 13px 13px 20px #cbced1, -13px -13px 20px #fff
        }
        .logo {
            width: 120px;
            margin: auto
        }
        .logo img {
            width: 100%;
            height: 80px;
            object-fit: cover;
            border-radius: 30%;
            box-shadow: 0px 0px 3px #5f5f5f, 0px 0px 0px 5px #ecf0f3, 8px 8px 15px #a7aaa7, -8px -8px 15px #fff
        }
        .wrapper .name {
            font-weight: 600;
            font-size: 1.4rem;
            letter-spacing: 1.3px;
            padding: 15px;
            text-align: center;
            color: #555
        }
        .wrapper .error {
            color: #FF0000;
            text-decoration: none;
            font-size: 0.8rem;
            text-align: center;
            padding: 5px;
        }
        .wrapper .text {
            text-decoration: none;
            font-size: 0.9rem;
            text-align: center;
            padding: 10px;
        }
        .wrapper a {
            text-decoration: none;
            font-size: 1rem;
            color: #03A9F4
        }
        .wrapper a:hover {
            color: #039BE5
        }
        @media(max-width: 380px) {
            .wrapper {
                margin: 30px 20px;
                padding: 40px 15px 15px 15px
            }
        }
    </style>
</head>
<body>
    <form name="login" method="post">
        <div class="wrapper">
            <div class="logo"> <img src="gvt.jpg" alt=""> </div>
            <div class="text-center mt-4 name">TALUK FRONT OFFICE</div>
            <form class="p-3 mt-3">
                <label>
                    <input type="text" placeholder="Username" name="username"/>
                </label>
                <label>
                    <input type="password" placeholder="Password" name="password" />
                </label>
                <button class="blue" type="submit" name="submit"><i class="material-icons">lock</i>Log in</button>
            </form>
            <div class="error"><?php echo $error; ?></div>
            <div class="text"> Don't have an Account?<a href="registration.php"> <i class="material-icons">how_to_reg</i>Register</a> </div>
        </div>
    </form>
</body>
</html>