package com.example.sree.talapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.telephony.SmsManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class dataAccess extends AsyncTask<Void,Void,String[]> {

    Context ctx;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected String[] doInBackground(Void... voids) {

        try {
            //url = new URL("https://192.168.43.80/filename");
            URL url2 = new URL("http://117.232.108.35:8086/www-test/ktfo-test/Enquiry/sms/");
            HttpTrustManager.allowAllSSL();
            HttpURLConnection connection = (HttpURLConnection) url2.openConnection();
            String filename;
            InputStream inputStream21 = connection.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream21));
            int i = 0;
            Scanner sc=new Scanner(bf);
            ArrayList<String> csvFiles = new ArrayList<String>();
            Matcher m = null;
            Pattern p = Pattern.compile("([^\"]*csv)");
            while(sc.hasNextLine()){
                String linee = sc.nextLine();
                Log.d("CSVFILES", linee);
               //"([^"]*csv)" //<a href=("[^"]*")[^<]*</a>   //\s*(?i)href\s*=\s*("([^"]*")|'[^']*'|([^'">\s]+))
                m = p.matcher(linee);
                if (m.find()) {
                    csvFiles.add(m.group());
                    i++;
                }
            }
            Log.d("CSVFILESSSS", String.valueOf(csvFiles));
            sc.close();

            Scanner sc2 = null;
            File path = new File(Environment.getExternalStorageDirectory().getPath());
            int cc=1;

            ArrayList<String> unListed=new ArrayList<>();
      //Read List.txt
            BufferedReader fbf = new BufferedReader(new InputStreamReader(new FileInputStream(path + "/List.txt"), StandardCharsets.UTF_8));
            for(String f : csvFiles) {
                boolean flag = false;
                filename = f;
                Log.e("FILENAME", filename);

                String str;
                while((str=fbf.readLine())!=null){
                    if (str.contains(filename)) {
                        flag = true;
                        break;
                    }
                    Log.d("FLAG str", String.valueOf(flag)+" and "+str);
                }
               /* Scanner lsc = new Scanner(new FileInputStream(path + "/List.txt"));
                while (lsc.hasNextLine()) {
                    String line2 = lsc.nextLine();
                    Log.d("LISTT", line2 + " " + filename);
                    if (line2.contains(filename)) {
                        flag=false;
                        break;
                    }
                    else
                        flag=true;
                }*/
                if (!flag)
                unListed.add(filename);
                //lsc.close();

            }
            fbf.close();
            Log.d("UNLISTED CSVs", String.valueOf(unListed));

			FileWriter writer = null;
            writer = new FileWriter(path+"/List.txt",true);
                    for(String s : unListed) {
                        File sf = new File("http://117.232.108.35:8086/www-test/ktfo-test/Enquiry/sms/" + s);
                        URI u = sf.toURI();
                        File ff = new File(u.toURL().toURI());
                        URL url = new URL(sf.toString());
                        HttpTrustManager.allowAllSSL();
                        HttpURLConnection connection2 = (HttpURLConnection) url.openConnection();
                        InputStream inputStream1 = connection2.getInputStream();
                        BufferedReader bf2 = new BufferedReader(new InputStreamReader(inputStream1));
                        sc2 = new Scanner(bf2);
                        sc2.useDelimiter(",");

                        writer.append(s);
                        writer.append("\n");
                      //  writer.flush();
                        ArrayList<String> msg = new ArrayList<String>();
                        ArrayList<String> mobile = new ArrayList<String>();
                        Log.d("COUNT", String.valueOf(cc++));
                        while (sc2.hasNextLine()) {
                            String line1 = sc2.nextLine();
                            String[] arr = line1.split(",", 2);
                            mobile.add(arr[0]);
                            msg.add(arr[1]);

                        }
                        sc2.close();
                        sendSMS(msg,mobile);
                    }
            writer.close();
        } catch (Exception e) {
           // e.printStackTrace();
            Log.e("ERROR", Log.getStackTraceString(e));
        }

        //delete file content
        return null;
    }

    private void sendSMS(ArrayList<String> msg, ArrayList<String> mobile) {
       // PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        SmsManager sm = SmsManager.getDefault();
        int count = mobile.size();
        Log.d("COUNT", String.valueOf(count));
        int i = 0;
        //while (count >= 1) {
            sm.sendTextMessage(mobile.get(i), null, msg.get(i), null, null);
       //     count--;
       //     i++;
      //  }

    }
}
