<?php
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;
include '../phpqrcode/qrlib.php';

session_start();
if(!isset($_SESSION['taluk_id']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
$taluk_id=$_SESSION['taluk_id'];
$result = $conn->query("SELECT * From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
$t_phn = $row['phn_no'];

$sql1='SELECT * FROM application ORDER BY CAST(application_number as int) DESC LIMIT 1';
	if($result1=$conn->query($sql1))
    {
        if($row1=$result1->fetch_assoc())
        {                   
            $application_no=$row1['application_number'];
            $file_no=$row1['file_number'];
            $section_id=$row1['section_submitted'];
            $purpose=$row1['application_subject'];
            $date=date('d/m/y', strtotime($row1['date_applied']));
            $person_id=$row1['person_id'];
            $sql2="SELECT * FROM person WHERE person_id='$person_id';";
            if($result2=$conn->query($sql2))
            {
                if($row2=$result2->fetch_assoc())
                {   
                    $name=$row2['name'];
                    $address1=$row2['address1'];
                    $address2=$row2['address2'];
                    $mobile_number=$row2['mobile_number'];                  
                }                    
            }
            $sql3="SELECT section_name FROM section WHERE section_id='$section_id' and taluk_id=$taluk_id;";
            if($result3=$conn->query($sql3))
            {
                if($row3=$result3->fetch_assoc())
                { 
                    $section_name=$row3['section_name'];
                } 
            } 
                       
        }                   
    }
    
$text = "http://localhost/taluks-fo-repo/public/result.php?application_number=".$application_no."&submit=submit"; 
$path = 'Resources/'; 
$image = $path."qrcode.png"; 

$ecc = 'L'; 
$pixel_Size = 100; 
$frame_Size = 2; 

QRcode::png($text,$image,$ecc,$pixel_Size,$frame_Size); 

function generatePDF($source, $output, $image, $taluk, $t_phn, $application_no, $file_no, $name, $house, $loc, $phn, $section, $purpose, $date) 
{
    $pdf = new FPDI('Landscape','mm',array(210,148)); // Array sets the X, Y dimensions in mm
    $pdf->AddPage();
    $pagecount = $pdf->setSourceFile($source);
    $tppl = $pdf->importPage(1);
 
    $pdf->useTemplate($tppl, 0, 0, 210, 148);

    $pdf->Image($image,67,22,31,31); // X start, Y start, X width, Y width in mm

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(36.247,27); // X start, Y start in mm
    $pdf->Write(0, $application_no);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(131.5,27); // X start, Y start in mm
    $pdf->Write(0, $application_no);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(36.247,44); // X start, Y start in mm
    $pdf->Write(0, $file_no);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(131.5,44); // X start, Y start in mm
    $pdf->Write(0, $file_no);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(53.445,56.62); // X start, Y start in mm
    $pdf->Write(0, $name);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(147.6375,56.62); // X start, Y start in mm
    $pdf->Write(0, $name);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(22.225,61.38); // X start, Y start in mm
    $pdf->Write(0, $house);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(117.475,61.38); // X start, Y start in mm
    $pdf->Write(0, $house);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(22.225,67.47); // X start, Y start in mm
    $pdf->Write(0, $loc);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(117.475,67.47); // X start, Y start in mm
    $pdf->Write(0, $loc);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(22.225,74); // X start, Y start in mm
    $pdf->Write(0, $phn);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(117.475,74); // X start, Y start in mm
    $pdf->Write(0, $phn);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(42.86,80.6979); // X start, Y start in mm
    $pdf->Write(0, $section);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(139,80.6979); // X start, Y start in mm
    $pdf->Write(0, $section);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(14.55,87.577); // X start, Y start in mm
    $pdf->Write(0, $purpose);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(108.479,87.577); // X start, Y start in mm
    $pdf->Write(0, $purpose);
 
    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(17.20,93.39); // X start, Y start in mm
    $pdf->Write(0, $date);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(111.654,93.39); // X start, Y start in mm
    $pdf->Write(0, $date);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(12.96,111.38); // X start, Y start in mm
    $pdf->Write(0, $date);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(107.42,111.38); // X start, Y start in mm
    $pdf->Write(0, $date);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(63.69,108.71); // X start, Y start in mm
    $pdf->Write(0, $taluk);

    $pdf->SetFont('Times','B',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB 
    $pdf->SetXY(158.77,108.71); // X start, Y start in mm
    $pdf->Write(0, $taluk);



    $pdf->Output($output, "I");
}
generatePDF("Resources/Receipt.pdf", "output.pdf", $image, $taluk, $t_phn, $application_no, $file_no, $name, $address1, $address2, 'Phn.no: '.$mobile_number,$section_name, $purpose, $date);
?>