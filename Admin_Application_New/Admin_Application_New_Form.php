<?php 
session_start();
if(!isset($_SESSION['taluk_id']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php" ;
	$taluk_id=$_SESSION['taluk_id'];
	$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
	$row = $result->fetch_assoc();
	$taluk = $row['taluk_name'];
	include "../header.php";
	include "../Footer.php";
	
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../form.css">
	<style>
		.wrapper{
			max-width: 55%;
		}
	</style>
</head>
<body>
	
	<div class="form">
	    <form name="form" method="POST">
		<div class="wrapper">
            <div class="text-center mt-4 name">NEW APPLICATION</div>
            <form class="p-3 mt-3">
	        		<label>
	        			<input type="text" name="file_no" id="file_no" value="" pattern="[A-Za-z0-9/-]+" placeholder="File Number">
                    </label>
					<label>
				        <input type="text" id="name" name="name" placeholder="Name*" required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
                    </label>
                    <label>
			        	<input type="text" id="house" name="house" placeholder="House Name/Number*" required>
                    </label>
					<label>
				        <input type="text" id="loc" name="loc" placeholder="Locality*" required>
                    </label>
					<label>
				        <input type="text" id="city" name="city" placeholder="City" value="">
                    </label>
					<label>    
					    <input type="text" id="vlg" name="vlg" placeholder="Village" value="">   
                    </label>
					<label>
				        <input type="text" id="po" name="po" placeholder="Post Office" value="">
                    </label>
					<label>
				        <input type="text" id="pc" name="pc" placeholder="Pincode" pattern="[0-9]{6}" title="Only 6 digits are allowed" value="">
                    </label>
					<label>
		        		<input type="tel" id="phn" name="phn" placeholder="Phone Number" pattern="[0-9]{10}" title="Only 10 digits are allowed" value="">
                    </label>
					<label>
		        		<input type="text" id="ln" name="ln" placeholder="Land Number" pattern="[0-9]+" title="Only digits are allowed" value="">
                    </label>
					<label>
		        		<input type="email" id="email" name="email" placeholder="Email Eg:abc@gmail.com" value="">
					</label>
					<label>
                       <?php
                            $sql="SELECT section_id, section_name from section where for_application='Y' and taluk_id=$taluk_id order by section_name;";
                            $result=$conn->query($sql);
                        ?>    
                        <select id="section" name="section" placeholder="Initial Section*" required title="Select an initial section">
                            <option value="">Initial Section*</option>
                        <?php                            
                            if($result->num_rows>0)
                                while($row=$result->fetch_assoc())
                                    echo '<option value="'.$row['section_id'].'">'.$row['section_name'].'</option>';                    
                        ?> 
                        </select>  
					</label> 
		        	<label>
		        		<input type="text" id="sub" name="sub" placeholder="Application Subject*" required value="">
		        	
					</label>
					<div class="btns">
				        <button type="reset" class="button"><i class="material-icons">restart_alt</i>Reset</button>
				        <button class="blue button" type="submit" name="submit" class="submit"><i class="material-icons">verified_user</i>Submit</button>
					</div>
            </form>
		<div>
    </form>		
    </div>
    <?php
        if (isset($_POST['submit'])) 
        {   
            $file_no=$_POST['file_no'];     	
        	$name=$_POST['name'];
        	$house=$_POST['house'];
        	$loc=$_POST['loc'];
        	$city=$_POST['city'];
        	$vlg=$_POST['vlg'];
        	$po=$_POST['po'];
        	$pc=intval($_POST['pc']);
        	$phn=$_POST['phn'];
        	$ln=$_POST['ln'];
        	$email=$_POST['email'];
        	$section=$_POST['section'];
        	$sub=$_POST['sub'];
        	date_default_timezone_set("Asia/Kolkata");
            $date=date("Y-m-d h:i:s",time());
        	

        	$sql5="SELECT person_id FROM person where name='$name' and address1='$house' and address2='$loc' and taluk_id=$taluk_id;";
            $result3=$conn->query($sql5);
            if($result3->num_rows>0)       		
        	{	
        		if($row3=$result3->fetch_assoc())
        		{
        			$person_id=intval($row3['person_id']);	
        		}
        		$sql2='SELECT application_number FROM application ORDER BY CAST(application_number as int) DESC LIMIT 1';
        		if($result2=$conn->query($sql2))
        	    {
        		    if($row2=$result2->fetch_assoc())
        		    {  
        			    $application_number=intval($row2['application_number'])+1;
        		        $sql4="INSERT INTO application (application_number, file_number, person_id, section_submitted, application_subject, date_applied, status, section_current,taluk_id) VALUES ('$application_number','$file_no','$person_id',$section,'$sub','$date','Fresh',$section,$taluk_id);";
        		    }
        	    }
        	    if($conn->query($sql4))
	            { 
	?>
		            <script type="text/javascript"> 
					    alert("Successful");
                        window.onload=function() 
                        {
                        	window.open('Admin_Application_New_Receipt.php');
                        }
					</script>
    <?php	  
                } 
                else
                {
	?>
                    <script type="text/javascript"> 
					    alert("Failed");
                        location.replace("Admin_Application_New_Form.php");
                    </script>
    <?php
                }
        	}
        	else
        	{
        		$sql1='SELECT person_id FROM person ORDER BY CAST(person_id as int) DESC LIMIT 1';
        	    $sql2='SELECT application_number FROM application ORDER BY CAST(application_number as int) DESC LIMIT 1';
     
        	    if($result1=$conn->query($sql1))        		
        	    {	
        		    if($row1=$result1->fetch_assoc())
        		    {
        			    $person_id=intval($row1['person_id'])+1;
        		        $sql3="INSERT INTO person (person_id, name, address1, address2, address3, post_office, pin_code, village, mobile_number, email_id, land_number,taluk_id) VALUES ('$person_id','$name','$house','$loc','$city','$po',$pc,'$vlg','$phn','$email','$ln',$taluk_id);";	
        		    }
        	    }	
        	
        	    if($result2=$conn->query($sql2))
        	    {
        		    if($row2=$result2->fetch_assoc())
        		    {
        			    $application_number=intval($row2['application_number'])+1;
        		        $sql4="INSERT INTO application (application_number, file_number, person_id, section_submitted, application_subject, date_applied, status, section_current,taluk_id) VALUES ('$application_number','$file_no','$person_id',$section,'$sub','$date','Fresh',$section,$taluk_id);";
        		    }
        	    }
        	    if($conn->query($sql3)&&$conn->query($sql4))
	            { 	            	
	?>
		            <script type="text/javascript"> 
					    alert("Successful");
                        window.onload=function() 
                        {
                        	window.open('Admin_Application_New_Receipt.php');
                        }
                    </script>   
    <?php	  
                } 
                else
                {
	?>
                    <script type="text/javascript"> 
					    alert("Failed");
                        location.replace("Admin_Application_New_Form.php");
                    </script>
    <?php
                }
        	}
        }
    ?> 	
</body>
</html>