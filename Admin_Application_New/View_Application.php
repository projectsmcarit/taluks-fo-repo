<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
    echo "<script>alert('Session Expired');</script>";
    echo '<script type="text/javascript">location.replace("../index.php");</script>';
}
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
?>
<!DOCTYPE html>
<html>

<head>
    <title>Enquiry List</title>
    <link rel="stylesheet" href="../form.css">
    <style type="text/css">
        .wrapper {
            max-width: 95%;
            min-height: 250px;
            margin: 80px auto;
            padding: 40px 30px 30px 30px;
            background-color: #ecf0f3;
            border-radius: 15px;
            box-shadow: 13px 13px 20px #cbced1, -13px -13px 20px #fff;
        }

        .material-icons {
            vertical-align: -15%;
            font-size: 18px;
        }

        .date {
            margin: 0;
        }

        input[type=date] {
            width: 95%;
        }

        .btns {
            padding: 0 250px;
        }

        #search {
            height: 59.41px;
            width: 150px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="heading">LIST OF APPLICATIONS</div>
        <form method="POST">
            <div class="btns">
                <label class="date">
                    <input type="date" name="date" id="date" value="<?php echo date('Y-m-d', time()) ?>" />
                </label>
                <button type="submit" name="search" id="search" class="blue button"><i class="material-icons">search</i>Search</button>
            </div>
        </form>

        <div id="list" style="width: 100%">
            <?php
            date_default_timezone_set("Asia/Kolkata");
            $date = date("Y-m-d", time());
            $result = $conn->query("SELECT * FROM application WHERE date_applied LIKE '$date%' AND taluk_id=$taluk_id");
            if ($result->num_rows > 0) {
            ?>
                <table class="table">
                    <tr>
                        <th>Application Number</th>
                        <th>Name</th>
                        <th>Application Subject</th>
                        <th></th>
                    </tr>
                    <?php
                    while ($row = $result->fetch_assoc()) {
                        $application_no = $row['application_number'];
                        $application_subject = $row['application_subject'];
                        $person_id = $row['person_id'];
                        $name = "";
                        if ($res = $conn->query("SELECT name from person where person_id='$person_id' AND taluk_id=$taluk_id"))
                            if ($row1 = $res->fetch_array())
                                $name = $row1['name'];
                    ?>
                        <tr>
                            <td><?php echo $application_no; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $application_subject; ?></td>
                            <td><a class="a" href="Application_Details.php?application_number=<?php echo $application_no; ?>"><i class="material-icons">table_view</i>View More</a></td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            <?php
            } else {
                echo "<div class='text'>Nothing to display</div>";
            }
            ?>
        </div>

    </div>

    <br>
</body>

</html>

<?php
if (isset($_REQUEST['search'])) {
    $date = date('Y-m-d', strtotime($_REQUEST['date']));

?>
    <script>
        document.getElementById('date').value = "<?php echo $date ?>";
    </script>
    <?php
    $result = $conn->query("SELECT * FROM application WHERE date_applied LIKE '$date%' AND taluk_id=$taluk_id;");

    if ($result->num_rows > 0) {
    ?>
        <script type="text/javascript">
            var content = '<table class="table"><tr><th>Application Number</th><th>Name</th><th>Application Subject</th><th></th></tr>';
        </script>
        <?php
        while ($row = $result->fetch_assoc()) {

            $application_no = $row['application_number'];
            $application_subject = $row['application_subject'];
            $person_id = $row['person_id'];
            $name = "";
            if ($res = $conn->query("SELECT name from person where person_id='$person_id' AND taluk_id=$taluk_id"))
                if ($row1 = $res->fetch_array())
                    $name = $row1['name'];
        ?>
            <script type="text/javascript">
                content += '<tr><td><?php echo $application_no; ?></td><td><?php echo $name; ?></td><td><?php echo $application_subject; ?></td><td><a class="a" href="Application_Details.php?application_number=<?php echo $application_no; ?>"><i class="material-icons">table_view</i>View More</a></td></tr>';
            </script>
        <?php
        }
        ?>
        <script>
            content += "</table>";
        </script>
        <?php
        
    } else {
        ?>
        <script>
            content = "<div class='text'>Nothing to display</div>";
        </script>
    <?php
    }
    ?>
    <script type="text/javascript">
        document.getElementById('list').innerHTML = content;
    </script>
<?php
}

?>