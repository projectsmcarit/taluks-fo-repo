<?php
session_start();

$application_no = $_GET['application_number'];
if (!isset($_SESSION['taluk_id'])) {
    echo "<script>alert('Session Expired');</script>";
    echo '<script type="text/javascript">location.replace("../index.php");</script>';
}

//Including database connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
$query = "SELECT * FROM application WHERE application_number='$application_no'";
$result = mysqli_query($conn, $query);
while ($row = $result->fetch_assoc()) {
    $application_no = $row['application_number'];
    $file_no = $row['file_number'];
    $section_submitted = $row['section_submitted'];
    $application_subject = $row['application_subject'];
    $person_id = $row['person_id'];

    if ($res = $conn->query("SELECT * from person where person_id='$person_id' AND taluk_id=$taluk_id"))
        while ($row1 = $res->fetch_assoc()) {
            $name = $row1['name'];
            $house = $row1['address1'];
            $locality = $row1['address2'];
            $city = $row1['address3'];
            $village = $row1['village'];
            $post_office = $row1['post_office'];
            $pin_code = isset($row1['pin_code']) ? $row1['pin_code'] : "";
            $mobile_no = isset($row1['mobile_number']) ? $row1['mobile_number'] : "";
            $land_no = isset($row1['land_number']) ? $row1['land_number'] : "";
            $email_id = isset($row1['email_id']) ? $row1['email_id'] : "";
?>
        <!DOCTYPE html>
        <html>

        <head>
            <title></title>
            <!--including css file-->
            <link rel="stylesheet" type="text/css" href="../form1.css">
            <style>
                .wrapper {
                    max-width: 55%;
                }
            </style>
        </head>

        <body>

            <div class="form">
                <form name="form" method="POST">
                    <div class="wrapper">
                        <div class="text-center mt-4 name">APPLICATION DETAILS</div>
                        <form class="p-3 mt-3">
                            <p>
                                <input type="text" name="application_no" id="application_no" value="<?php echo $application_no; ?>" pattern="[A-Za-z0-9/-]+" placeholder=" " disabled>
                                <label>Application Number*</label>
                            </p>
                            <p>
                                <input type="text" name="file_no" id="file_no" value="<?php echo $file_no; ?>" pattern="[A-Za-z0-9/-]+" placeholder=" " disabled>
                                <label>File Number</label>
                            </p>
                            <p>
                                <input type="text" id="name" name="name" value="<?php echo $name; ?>" placeholder=" " required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable" disabled>
                                <label>Name*</label>
                            </p>
                            <p>
                                <input type="text" id="house" name="house" value="<?php echo $house; ?>" placeholder=" " required disabled>
                                <label>House Name/Number*</label>
                            </p>
                            <p>
                                <input type="text" id="loc" name="loc" value="<?php echo $locality; ?>" placeholder=" " required disabled>
                                <label>Locality*</label>
                            </p>
                            <p>
                                <input type="text" id="city" name="city" value="<?php echo $city; ?>" placeholder=" " value="" disabled>
                                <label>City</label>
                            </p>
                            <p>
                                <input type="text" id="vlg" name="vlg" value="<?php echo $village; ?>" placeholder=" " value="" disabled>
                                <label>Village</label>
                            </p>
                            <p>
                                <input type="text" id="po" name="po" value="<?php echo $post_office; ?>" placeholder=" " value="" disabled>
                                <label>Post Office</label>
                            </p>
                            <p>
                                <input type="text" id="pc" name="pc" value="<?php echo $pin_code; ?>" placeholder=" " pattern="[0-9]{6}" title="Only 6 digits are allowed" value="" disabled>
                                <label>Pincode</label>
                            </p>
                            <p>
                                <input type="tel" id="phn" name="phn" value="<?php echo $mobile_no; ?>" placeholder=" " pattern="[0-9]{10}" title="Only 10 digits are allowed" value="" disabled>
                                <label>Phone Number</label>
                            </p>
                            <p>
                                <input type="text" id="ln" name="ln" value="<?php echo $land_no; ?>" placeholder=" " pattern="[0-9]+" title="Only digits are allowed" value="" disabled>
                                <label>Land Number</label>
                            </p>
                            <p>
                                <input type="email" id="email" name="email" value="<?php echo $email_id; ?>" placeholder=" " value="" disabled>
                                <label>Email</label>
                            </p>
                            <p>
                                <?php
                                $sql = "SELECT section_id, section_name from section where for_application='Y' and taluk_id=$taluk_id order by section_name;";
                                $result = $conn->query($sql);
                                ?>
                                <select id="section" name="section" placeholder="Initial Section*" required title="Select an initial section" disabled>
                                    <option value="">Initial Section*</option>
                                    <?php
                                    if ($result->num_rows > 0)
                                        while ($row = $result->fetch_assoc())
                                            if ($row['section_id'] == $section_submitted)
                                                echo '<option selected value="' . $row['section_id'] . '">' . $row['section_name'] . '</option>';
                                            else
                                                echo '<option value="' . $row['section_id'] . '">' . $row['section_name'] . '</option>';
                                    ?>
                                </select>
                                <label>Section*</label>
                            </p>
                            <p>
                                <input type="text" id="sub" name="sub" value="<?php echo $application_subject; ?>" placeholder=" " required value="" disabled>
                                <label>Application Subject*</label>
                            </p>

                        </form>
                        <div>
                </form>
            </div>
        </body>

        </html>
<?php
        }
}
?>