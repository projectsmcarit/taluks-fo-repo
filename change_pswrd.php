<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "connection.php" ; 
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
?>
<!DOCTYPE html>
<html>
<head>
<title>Change Password</title>

<!--styles file including-->
<link rel="stylesheet" href="form.css">
</head>
<body>
<!--Header-->
<?php
include "header.php";
?>

<!--form to add new section-->
<div class="wrapper">
      <div class="text-center mt-4 name">CHANGE YOUR PASSWORD</div>
      <form class="p-3 mt-3" method="post" id="change_pswd" name="change_pswd" action="" onSubmit="return submit_form()">
		<label>
      		<input type="text" name="user_name" placeholder="User Name *" required="required"/>
		</label>
		<label>
      		<input type="password" name="crnt_pswd" placeholder="Current Password *" required="required"/>
		</label>
      	<label>
      		<input type="password" id="new_pswd" name="new_pswd" placeholder="New Password *" required="required"/>
		</label>
	  	<label>
      		<input type="password" id="cnfm_pswd" name="cnfm_pswd" placeholder="Confirm Password *" required="required" />
		</label>
		<div class="btns">
            <button type="cancel" class="button" onClick="window.location='Admin_Home.php';return false;"><i class="material-icons">restart_alt</i>Cancel</button>
            <button class="blue button" type="submit" name="change"><i class="material-icons">done</i>Change</button>
         </div>
</form>
<br><br><br><br>
</div>
<div style="position:fixed; bottom:0; width:100%;">
<?php
//including footer file
include "Footer.php";
?> 
</body>
</html>
<?php
 if(isset($_POST['change']))
 {
 	$user_name=$_POST['user_name'];
	$crnt_pswd=$_POST['crnt_pswd'];
	$new_pswd=$_POST['new_pswd'];
	$cnfm_pswd=$_POST['cnfm_pswd'];
	//insert new section to table section
	$sql="select * from login where username='$user_name' and password='$crnt_pswd'";
	$res=mysqli_query($conn,$sql);
	
	if(mysqli_num_rows($res)>0)
	{
		if($new_pswd==$cnfm_pswd)
		{
			$sql2="UPDATE login SET password='$cnfm_pswd' WHERE username='$user_name'";
			if(mysqli_query($conn,$sql2))
			{?>
				<script>alert('Password Changed!');</script>
				<script type="text/javascript">
				location.replace("index.php");
				</script>
			<?php
			}
			else
			{
				?>
				<script>alert("Something Wrong!!!! Password doesn't Changed.Try Again...");</script>
				<script type="text/javascript">
				location.replace("Admin_Home.php");
				</script>
				<?php
			}
		}
		else
		{?>
			<script>
			alert("Password doesn't match...!")
			location.replace("change_pswrd.php");
		   </script>
		   <?php
		}

	}
	else
	{?>
		<script>alert("Something Wrong!!!! Username or Password incorrect");</script>
		<script type="text/javascript">
		location.replace("change_pswrd.php");
		</script>
		<?php
	}
	
 }
?>