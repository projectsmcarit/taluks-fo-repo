<?php 
session_start();
if(!isset($_SESSION['taluk_id']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//including database connection file
include "connection.php" ; 
$taluk_id=$_SESSION['taluk_id'];

$result=$conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id"); 
$row=$result->fetch_assoc();
$taluk=$row['taluk_name'];
//$section_id=$_GET['section_id'];
//fetching data from table section
$qry="select * from taluk where taluk_id='$taluk_id'";
$result=$conn->query($qry);
if($result->num_rows>0)
{
	while($data=$result->fetch_assoc())
	{

?>
<!DOCTYPE html>
<html>
<head>
<title>Edit_Profile</title>
<link rel="stylesheet" href="form.css">
</head>
<body>
	<?php
	include "header.php";
	?>
<script>
	<!--confirmation message before delete section-->
	function update()
	{
		return confirm("Do you really want to update");
	}
	<!--validation to enter Y / N for_application field-->
</script>
<!--Header-->
<div class="wrapper">
      <div class="text-center mt-4 name">EDIT PROFILE</div>
      <form class="p-3 mt-3" method="post" id="section_edit" name="section_edit" action="" onSubmit="return update()">
		<label>Taluk * :
	        <input type="text" name="taluk" id="taluk" pattern="[A-Za-z]+" required value="<?php echo $data['taluk_name'];?>">
		</label>
	    <label>Address * :
            <textarea name="address" id="address"  cols="30" rows="4" pattern="[A-Za-z0-9/-]+" required ><?php echo $data['address'];?></textarea>
		</label>
	    <label>Phone Number * :
	        <input type="text" name="phn_no" id="phn_no" value="<?php echo $data['phn_no'];?>" pattern="[0-9]{10}" required>
		</label>
	    <label>Email ID :
	        <input type="email" name="email" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="<?php echo $data['email_id'];}?>">
		</label>
		<div class="btns">
            <button type="reset" class="button" name="cancel" onClick="window.location='Admin_Home.php';return false;"><i class="material-icons">restart_alt</i>Cancel</button>
            <button class="blue button" type="submit" name="edit"><i class="material-icons">done</i>Update</button>
         </div>
</form>
</div>

<div style=" bottom:0; width:100%;">
<?php
    }
//including footer file
include "Footer.php";
?> 
</div>
</body>
</html>
<?php
 if(isset($_POST['edit']))
 {
 	$name=$_POST['taluk'];
	$address=$_POST['address'];
	$phn_no=$_POST['phn_no'];
	$email=$_POST['email'];
	//updating section_name 
	$sql="update taluk set taluk_name='$name' , address='$address', phn_no='$phn_no', email_id='$email' where taluk_id='$taluk_id'";
	
	if($conn->query($sql)== TRUE)
	 	{ 
	?>
	<script>
		alert(" Updated Successfully");
		location.replace("Admin_Home.php");
	</script> 
	 <?php
         }
	else
		{
	?>
  	<script> 
		alert("failed");
		location.replace("Edit_Profile.php");
	</script>  
<?php
		}
}

?>