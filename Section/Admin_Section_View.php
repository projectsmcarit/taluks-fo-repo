<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//Including database connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
?>
<!DOCTYPE html>
<html>

<head>
	<title>Admin_Section_View</title>
	<link rel="stylesheet" href="../form.css">
	<style>
		.wrapper {
			max-width: 95%;
		}
	</style>
</head>

<body>
	<!--Header-->
	<?php
	include "../header.php";
	?>

	<!--section view form-->
	<div class="wrapper">
		<form id="section_view" name="section_view" method="post" action="">
			<table class="table">
				<tr>
					<div class="table th">
						<th>Section ID</th>
						<th>Section Name</th>
						<th>For Application</th>
						<th>For Pass</th>
						<th>Purpose</th>
						<th colspan="2"></th>
					</div>
				</tr>
				<?php
				//fetching datas from table section
				$records = mysqli_query($conn, "select * from section where taluk_id='$taluk_id'");

				while ($data = mysqli_fetch_array($records)) {
				?>
					<tr>
						<td><?php echo $data['section_id']; ?></td>
						<td><?php echo $data['section_name']; ?></td>
						<td><?php echo $data['for_application']; ?></td>
						<td><?php echo $data['for_pass']; ?></td>
						<td><?php echo $data['section_purpose']; ?></td>
						<td><a href="Admin_Section_Edit.php?section_id=<?php echo $data['section_id']; ?>" class="a"><i class="material-icons">edit</i>Edit</a></td>
						<td><a href="Admin_Section_Delete.php?section_id=<?php echo $data['section_id']; ?>" class="a" onClick="return confirm('Are you sure to delete ?');"><i class="material-icons">delete</i>Delete</a>
					</tr>
				<?php
				}
				?>
			</table>
		</form>
	</div>
	<?php
	//including footer file
	include "../Footer.php";
	?>
</body>

</html>