<?php 
session_start();
if(!isset($_SESSION['taluk_id']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//including database connection file
include "../connection.php" ; 
$taluk_id=$_SESSION['taluk_id'];
$result=$conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id"); 
$row=$result->fetch_assoc();
$taluk=$row['taluk_name'];
$section_id=$_GET['section_id'];
//fetching data from table section
$qry="select * from section where section_id='$section_id' and taluk_id='$taluk_id'";
$result=$conn->query($qry);
if($result->num_rows>0)
{
	while($data=$result->fetch_assoc())
	{

?>
<!DOCTYPE html>
<html>
<head>
<title>Admin_Section_Edit</title>
<link rel="stylesheet" href="../form1.css">
</head>
<body>
	<?php
	include "../header.php";
	?>
<script>
	//validation to enter Y / N for_application field
	function YN_validation(a)
	{
		var value1= document.getElementById(a).value;
		if(value1=="Y" || value1=="N") {
			if (a=='for_application') {
				document.getElementById('one').innerHTML="";
			}
			else {
				document.getElementById('two').innerHTML="";
			}
		}
		else {
			document.getElementById(a).value="";
			if (a=='for_application') {
				document.getElementById('one').innerHTML="You should enter either Y or N";
			}
			else {
				document.getElementById('two').innerHTML="You should enter either Y or N";
			}
		}
	}
</script>
<div class="wrapper">
    <div class="text-center mt-4 name">EDIT SECTION</div>
	<form class="p-3 mt-3"  id="section_edit" name="section_edit" method="post" action="" onSubmit="return confirm('Do you really want to update')">
  	<p>
      	<input type="text" name="section_id" disabled="disabled"  value=" <?php echo $data['section_id'];?>"/>
		<label>Section ID</label>
	</p>
	<p>
      	<input type="text" name="section_name"  value="<?php echo $data['section_name'];?>" required />
		<label>Section Name*</label>
	</p>
	<p>
      	<input type="text" id="for_application" name="for_application" value="<?php echo $data['for_application'];?>" onkeyup="YN_validation(this.id)" required />
		<label>For Application (Y/N)*</label>
	</p>
	<div class="error" id="one"></div>
	<p>
      <input type="text" id="for_pass" name="for_pass" onkeyup="YN_validation(this.id)"  value="<?php echo $data['for_pass'];?>"/>
	  <label>For Pass (Y/N)*</label>
	</p>
	<div class="error" id="two"></div>
	<p>
      <textarea name="purpose" cols="5" value="<?php echo $data['section_purpose']; }}?>" rows="5"></textarea>
	  <label>Section Purpose</label>
	</p>
	
    
	<!--td colspan="2" align="center"><button type="reset" name="cancel" onClick="window.location='Admin_Section_View.php';return false;">CANCEL</button>
	  	 <button type="submit" name="edit">UPDATE</button></td>
    </tr-->
	<div class="btns">
            <button type="reset" class="button" name="cancel" onClick="window.location='Admin_Section_View.php';"><i class="material-icons">close</i>Cancel</button>
            <button class="blue button" type="submit" name="edit"><i class="material-icons">update</i>Update</button>
         </div>
</form>
</div>

<div style=" bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?> 
</div>
</body>
</html>
<?php
 if(isset($_POST['edit']))
 {
 	$section=$_POST['section_name'];
	$for_application=$_POST['for_application'];
	$for_pass=$_POST['for_pass'];
	$purpose=$_POST['purpose'];
	//updating section_name 
	$sql="update section set section_name='$section' , for_application='$for_application', for_pass='$for_pass', section_purpose='$purpose' where section_id='$section_id'";
	
	if($conn->query($sql)== TRUE)
	 	{ 
	?>
	<script>
		alert(" Updated Successfully");
		location.replace("Admin_Section_View.php");
	</script> 
	 
	else
		{
	?>
  	<script> 
		alert("failed");
		location.replace("Admin_Section_View.php");
	</script>  
<?php
		}
}
?>