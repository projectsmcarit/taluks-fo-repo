<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
?>
<!DOCTYPE html>
<html>

<head>
	<style>
		.wrapper {
			max-width: 55%;
		}
	</style>
	<title>Admin_Section_Add</title>
	<!--styles file including-->
	<link rel="stylesheet" href="../form1.css">
</head>
<script type="text/javascript">
	// validation to enter Y / N for_application field
	function YN_validation(a) {
		var value1 = document.getElementById(a).value;
		if (value1 == "Y" || value1 == "N") {
			if (a == 'for_application') {
				document.getElementById('one').innerHTML = "";
			} else {
				document.getElementById('two').innerHTML = "";
			}
		} else {
			document.getElementById(a).value = "";
			if (a == 'for_application') {
				document.getElementById('one').innerHTML = "You should enter either Y or N";
			} else {
				document.getElementById('two').innerHTML = "You should enter either Y or N";
			}
		}
	}
</script>

<body>
	<?php
	include "../header.php";
	include "../Footer.php";
	?>
	<div class="wrapper">
		<div class="text-center mt-4 name">NEW SECTION</div>
		<form class="p-3 mt-3" method="post" id="section_add" name="section_add" action="" onSubmit="return confirm('Do you really want to add')">
			<p>
				<input type="text" name="section_name" placeholder=" " required="required" />
				<label>Section Name*</label>
			</p>
			<p>
				<input type="text" id="for_application" name="for_application" placeholder=" " required="required" onkeyup="YN_validation(this.id)" />
				<label>For Application (Y/N)*</label>
			</p>
			<div class="error" id="one"></div>
			<p>
				<input type="text" id="for_pass" name="for_pass" required="required" placeholder=" " onkeyup="YN_validation(this.id)" />
				<label>For Pass (Y/N)*</label>
			</p>
			<div class="error" id="two"></div>
			<p>
				<textarea name="purpose" placeholder=" " cols="5" rows="5"></textarea>
				<label>Section Purpose</label>
			</p>
			<div class="btns">
				<button type="reset" class="button"><i class="material-icons">restart_alt</i>Reset</button>
				<button class="blue button" type="submit" name="add"><i class="material-icons">done</i>Submit</button>
			</div>
		</form>
	</div>
	</div>
	<div style="bottom:0; width:100%;">
</body>

</html>
<?php
if (isset($_POST['add'])) {
	$section_name = $_POST['section_name'];
	$for_application = $_POST['for_application'];
	$for_pass = $_POST['for_pass'];
	$purpose = $_POST['purpose'];

	//insert new section to table section
	$sql = "insert into section(section_name,for_application,for_pass,section_purpose,taluk_id)values('$section_name','$for_application','$for_pass','$purpose','$taluk_id')";

	if ($conn->query($sql) === TRUE) {
?>
		<script>
			alert("Added Successfully");
			location.replace("Admin_Section_View.php");
		</script>
	<?php
	} else {
	?>
		<script>
			alert("failed");
			location.replace("Admin_Section_View.php");
		</script>
<?php
	}
}
?>