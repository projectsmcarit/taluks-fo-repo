<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width,initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link rel="stylesheet" href="bootstrap.css">
   <link rel="stylesheet" href="main.css">
   <style type="text/css">
      button
      {
         border-radius: 10px;
      }
      button:hover
      {
         background-color: limegreen;
         color: white;
      }
   </style>   
</head>
<body>
   <center>
      <h1>Your application status...</h1>
   </center>
   <center>
      <table style="width:50%">
      <?php
         include "../connection.php";
         if(isset($_GET['submit']))
         {
            $application_number=$_GET['application_number'];
            $result1=$conn->query("SELECT * FROM application WHERE application_number='$application_number'");
            $row1=$result1->fetch_assoc();
            $taluk_id=$row1['taluk_id'];
      ?>
            <tr>
               <th><i>Application Number:</i></th>
               <td><?php echo $row1['application_number']?><br></td>
            </tr>
      <?php
            $result2=$conn->query("SELECT name FROM person WHERE person_id=(SELECT person_id FROM application WHERE application_number='$application_number')");
            $row2=$result2->fetch_assoc();
      ?>
            <tr>
               <th><i>Name of Applicant:</i></th>
               <td><?php echo $row2['name']?><br></td>
            </tr>
            <tr>
               <th><i>Application Subject:</i></th>
               <td><?php echo $row1['application_subject']?><br></td>
            </tr>
            <tr>
               <th><i>Applied date:</i></th>
               <td><?php echo date('d-m-Y',strtotime($row1['date_applied'])) ?><br></td>
            </tr>
            <tr>
               <th><i>Status</i></th>
               <td><?php echo $row1['status']?><br></td>
            </tr>
      <?php   
            $result3=$conn->query("SELECT section_name FROM section WHERE taluk_id=$taluk_id and section_id=(SELECT section_submitted FROM application WHERE application_number='$application_number')");
            $row3=$result3->fetch_assoc();
      ?>
            <tr>
               <th><i>Section submitted:</i></th>
               <td><?php echo $row3['section_name']?><br></td>          
            </tr>
      <?php
            $result4=$conn->query("SELECT section_name FROM section WHERE taluk_id=$taluk_id and section_id=(SELECT section_current FROM application WHERE application_number='$application_number')");
            $row4=$result4->fetch_assoc();           
      ?>
            <tr>
               <th><i>Current Section:</i></th>
               <td><?php echo $row4['section_name']?><br></td>          
            </tr>
      <?php 
         }
      ?>
      </table>
   </center>
   <br> 
   <center>
      <div style="text-align: center; width: 50%;">
         <a href="search.php"><button type="submit" name="back">TRACK ANOTHER</button></a>        
      </div>
   </center>  
   <div style="position:fixed; bottom:0; width:100%;">
         <?php
         //including footer file
         include "../Footer.php";
         ?> 
   </div>
</body>
</html>
