<?php
include("connection.php");
global $error;
if (isset($_POST['submit'])) {
    $taluk = strtoupper($_POST['taluk']);
    $address = $_POST['address'];
    $phn_no = $_POST['phn_no'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $res = $conn->query("SELECT taluk_id FROM taluk WHERE taluk_name='$taluk'");
    if ($res->num_rows) {
        $error = "Account already existed";
    } else {
        $sql1 = "INSERT INTO taluk (taluk_name, address, phn_no, email_id) VALUES ('$taluk', '$address', '$phn_no', '$email')";
        if ($conn->query($sql1)) {
            $sql2 = "SELECT taluk_id FROM taluk ORDER BY taluk_id DESC LIMIT 1";
            if ($res1 = $conn->query($sql2)) {
                if ($row1 = $res1->fetch_assoc()) {
                    $taluk_id = $row1['taluk_id'];
                    $sql3 = "INSERT INTO login (username, password, taluk_id) VALUES ('$username', '$password', '$taluk_id')";
                }
            }
        }
        if ($conn->query($sql3)) {
            mkdir("Enquiry/" . $taluk);
            $file = fopen("Enquiry/" . $taluk . "/text.txt", "w");
            fwrite($file, "Hello World!!");
            fclose($file);
?>
            <script>
                alert("Registration Successful");
                location.replace("index.php");
            </script>
<?php
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Register</title>
    <script type="text/javascript">
        function preventForward() {
            window.history.forward();
        }
        setTimeout("preventBack()", 5 * 60 * 1000);
        window.onunload = function() {
            null
        };
    </script>
    <script>
        function validation() {
            var pswd = document.getElementById('password').value;
            var re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
            if (pswd.match(re)) {
                document.getElementById('one').innerHTML = '';
                document.getElementById('cpassword').pattern = pswd;
            } else {
                document.getElementById('one').innerHTML = 'The password must contain at least 1 lowercase letter, at least 1 uppercase letter, at least 1 digit, at least 1 special character(!@#$%^&*) and minimum 8 characters';
            }
        }

        function confirmation() {
            var pswd1 = document.getElementById('password').value;
            var pswd2 = document.getElementById('cpassword').value;
            if (pswd1 == pswd2) {
                document.getElementById('two').innerHTML = '';
            } else {
                document.getElementById('two').innerHTML = "The password doesn't match";
            }
        }
    </script>
    <link rel="stylesheet" href="form1.css">
    <style>
        .wrapper {
            max-width: 40%;
        }

        .btns {
            padding: 0 75px;
        }
    </style>
</head>

<body>

    <div class="wrapper">
        <a href="index.php"><button type="button" class="button" style="float: right;"><i class="material-icons">close</i></button></a>
        <div class="logo"> <img src="gvt.jpg" alt=""> </div>
        <div class="text-center mt-4 name">TALUK FRONT OFFICE REGISTRATION</div>
        <form class="p-3 mt-3" id="form" method="POST">
            <p>
                <input type="text" placeholder=" " name="taluk" id="taluk" pattern="[A-Za-z]+" required />
                <label>Taluk*</label>
            </p>
            <div class="error"><?php echo $error; ?></div>
            <p>
                <textarea placeholder=" " name="address" id="address" cols="30" rows="3" pattern="[A-Za-z0-9/-]+" required></textarea>
                <label>Address*</label>
            </p>
            <p>
                <input type="text" placeholder=" " name="phn_no" id="phn_no" pattern="[0-9 ]+" title="Enter a valid Phone Number" required />
                <label>Phone Number*</label>
            </p>
            <p>
                <input type="email" placeholder=" " name="email" id="email">
                <label>Email ID</label>
            </p>
            <p>
                <input type="text" placeholder=" " name="username" id="username" value="" pattern="[A-Za-z0-9]+" required />
                <label>Username*</label>
            </p>
            <p>
                <input type="password" placeholder=" " name="password" id="password" pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}" required onkeyup="validation()" />
                <label>Password*</label>
            </p>
            <div class="error" id="one"></div>
            <p>
                <input type="password" placeholder=" " name="cpassword" id="cpassword" value="" title="Password doesn't match" required onkeyup="confirmation()" />
                <label>Re-enter Password*</label>
            </p>
            <div class="error" id="two"></div>
            <div class="btns">
                <a href="index.php"><button type="button" class="button"><i class="material-icons">close</i>Cancel</button></a>
                <button class="blue button" type="submit" name="submit"><i class="material-icons">how_to_reg</i>Register</button>
            </div>
        </form>
    </div>
</body>

</html>