<?php
session_start();
if(!isset($_SESSION['taluk_id']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
?>
<!DOCTYPE html>
<html>
<head></head>
<body>
<?php
$taluk_id=$_SESSION['taluk_id'];
include "../connection.php" ; 
$output = '';
if(isset($_POST["query"]))
{
   $search = mysqli_real_escape_string($conn, $_POST["query"]);
   $query = "SELECT ap.application_number,ap.person_id,ap.application_subject,ap.status,ap.section_current,pe.name,pe.mobile_number from application ap JOIN person pe ON ap.person_id=pe.person_id 
   WHERE ap.application_number LIKE '%".$search."%'
   OR ap.person_id LIKE '%".$search."%'  
   OR ap.status LIKE '%".$search."%' 
   OR ap.section_current LIKE '%".$search."%' 
   OR pe.name LIKE '%".$search."%'  
   OR pe.mobile_number LIKE '%".$search."%'
   AND ap.taluk_id=$taluk_id";
}
else
{
   $query = "SELECT ap.application_number,ap.person_id,ap.application_subject,ap.status,ap.section_current,pe.name,pe.mobile_number FROM application ap JOIN person pe WHERE ap.person_id=pe.person_id";
}
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0)
{
?>
   <div>
      <table class="table">
         <tr>
            <th>Application No</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Action</th>
         </tr>
<?php
   while($row = mysqli_fetch_array($result))
   {
 	   $cur_sec=$row['section_current'];
 ?>
         <tr>
            <td><?php echo $row['application_number']; ?> </td>
            <td><?php echo $row['name']; ?> </td>
            <td><?php echo $row['mobile_number']; ?> </td>
            <td>
               <form method="POST" action="Admin_Application_Edit.php" class="btnedit" >
                  <input type="hidden" name="application_number" value="<?php echo $row['application_number'];?>">
                  <button type="submit" class="a" name="edit" style="border: none; background: none;"><i class="material-icons">edit</i>Edit</button>
               </form>
            </td>           
         </tr>
<?php
   }
}
else
{
   echo 'Data Not Found';
}
?>
