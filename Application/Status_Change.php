<?php
session_start();

$application_number = $_SESSION['application_number']; // application number for retriving personal details

if (!isset($_SESSION['taluk_id'])) {
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";


$query = " select * from application where application_number='" . $application_number . "'";
$result = mysqli_query($conn, $query);
while ($row = mysqli_fetch_assoc($result)) {
	$person_id = $row['person_id'];
	$stats = $row['status'];


?>
	<!DOCTYPE html>
	<html>

	<head>
		<title>Update Application Status</title>
		<link rel="stylesheet" href="../form1.css">
	</head>

	<body>
		<div class="wrapper">
		<div class="heading">UPDATE STATUS</div>
			<form method="post">
				<p>
					<input name="application_number" placeholder=" " type="text" disabled="disabled" value="<?php echo $application_number; ?>">
					<label>Application Number</label>
				</p>
				<p>
					<?php
					$qry = "select * from person where person_id='$person_id'";
					$res = mysqli_query($conn, $qry);

					while ($data = mysqli_fetch_assoc($res)) {
						$name = $data['name'];
					?>
						<input name="name" type="text" placeholder=" " disabled="disabled" value="<?php echo $name; ?>">
						<label>Name</label>
				</p>
				<p>
					<input type="text" list="status" placeholder=" " name="status" id="stat" required value="<?php echo $stats;}} ?>">
					<label>Status*</label>
					<datalist id="status">
						<option value="Additional document required">
						<option value="DCs permission needed">
						<option value="Disposed">
						<option value="Ordered">
						<option value="Processing">
						<option value="Site inspection required">
						<option value="Surveyors report awaited">
						<option value="V.O report awaited">
						<option value="With Head Surveyor">
						<option value="With JS">
						<option value="With Other Office">
						<option value="With Tahsildar">
						<option value="With village office">
					</datalist>
				</p>
				<div style="text-align: center;">
					<button name="update" type="submit" class="button blue"><i class="material-icons">update</i>Update</button>
				</div>
			</form>
		</div>
	</body>

	</html>
	<?php
	if (isset($_POST['update'])) {
		$status = $_POST['status'];

		$sql = "update application set status='$status' where application_number='" . $application_number . "'";
		if ($conn->query($sql) == TRUE) {
	?>
			<script>
				confirm(" Updated Successfully");
				location.replace('Application_Update_Status.php');
			</script>
		<?php
			//header('location:Application_Update_Status.php');  
		} else {
		?>
			<script>
				alert("failed");
			</script>
	<?php
		}
	}
	?>