<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
  echo "<script>alert('Session Expired');</script>";
  echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";

?>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Search</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="../form1.css">
  <style>
	  .wrapper {
			max-width: 80%;
			min-height: 300px;
		}
  </style>
</head>

<body>
  <!--Header-->

  <div class="wrapper">
  	<div class="heading">SEARCH APPLICATION</div>
    <p>
        <input type="text" name="search_text" id="search_text" placeholder=" " class="form-control" />
		<label><i class="material-icons">search</i>Search Here</label>
	</p>
    <div id="result"></div>
  </div>
</body>

</html>


<script>
  $(document).ready(function() {

    load_data();

    function load_data(query) {
      $.ajax({
        url: "fetch.php",
        method: "POST",
        data: {
          query: query
        },
        success: function(data) {
          $('#result').html(data);
        }
      });
    }
    $('#search_text').keyup(function() {
      var search = $(this).val();
      if (search != '') {
        load_data(search);
      } else {
        load_data();
      }
    });
  });
</script>