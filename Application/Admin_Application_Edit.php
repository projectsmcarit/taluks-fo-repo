<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
   echo "<script>alert('Session Expired');</script>";
   echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";

?>

<!DOCTYPE html>
<html>

<head>
   <title>Edit Application</title>
   <!--styles file including-->
   <link rel="stylesheet" href="../form1.css">
</head>

<body>

   <div class="wrapper">
      <div class="heading">EDIT APPLICATION</div>
      <?php
      if (isset($_POST['edit'])) {
         $application_number = $_POST['application_number'];
         $query1 = "SELECT application_subject, date_applied FROM application WHERE application_number='$application_number'";
         $result1 = $conn->query($query1);
         $query2 = "SELECT * FROM person WHERE person_id=(SELECT person_id FROM application WHERE application_number='$application_number')";
         $result2 = $conn->query($query2);
         if ($row1 = $result1->fetch_assoc()) {
            $application_subject = $row1['application_subject'];
            $date_applied = $row1['date_applied'];
         }
         if ($row2 = $result2->fetch_assoc()) {
            $person_id = $row2['person_id'];
            $name = $row2['name'];
            $mobile_number = $row2['mobile_number'];
            $address1 = $row2['address1'];
            $address2 = $row2['address2'];
            $address3 = $row2['address3'];
            $post_office = $row2['post_office'];
            $pin_code = $row2['pin_code'];
            $village = $row2['village'];
            $email_id = $row2['email_id'];
            $land_number = $row2['land_number'];
         }
      }
      ?>
      <form method="POST">
         <p>
            <input type="text" name="application_number" placeholder=" " value="<?php echo (isset($application_number) && !empty($application_number)) ? $application_number : ''; ?>" readonly>
            <label>Application Number</label>
         </p>
         <p>
            <input type="text" name="application_subject" placeholder=" " required value="<?php echo (isset($application_subject) && !empty($application_subject)) ? $application_subject : ''; ?>">
            <label>Application Subject*</label>
         </p>
         <p>
            <input type="date" name="date_applied" placeholder=" " disabled="disabled" value="<?php echo (isset($date_applied) && !empty($date_applied)) ? date('Y-m-d', strtotime($date_applied)) : ''; ?>">
            <label>Date Applied</label>
         </p>
         <p>
            <input type="text" name="name" placeholder=" " required value="<?php echo (isset($name) && !empty($name)) ? $name : ''; ?>" pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
            <label>Name*</label>
         </p>
         <p>
            <input type="text" name="address1" placeholder=" " required value="<?php echo (isset($address1) && !empty($address1)) ? $address1 : ''; ?>">
            <label>House Name/Number*</label>
         </p>
         <p>
            <input type="text" name="address2" placeholder=" " required value="<?php echo (isset($address2) && !empty($address2)) ? $address2 : ''; ?>">
            <label>Locality*</label>
         </p>
         <p>
            <input type="text" name="address3" placeholder=" " value="<?php echo (isset($address3) && !empty($address3)) ? $address3 : ''; ?>">
            <label>City</label>
         </p>
         <p>
            <input type="text" name="vlg" id="vlg" placeholder=" " value="<?php echo (isset($village) && !empty($village)) ? $village : ''; ?>">
            <label>Village</label>
         </p>

         <p>
            <input type="text" name="post_office" placeholder=" " value="<?php echo (isset($post_office) && !empty($post_office)) ? $post_office : ''; ?>">
            <label>Post Office</label>
         </p>
         <p>
            <input type="text" name="pin_code" placeholder=" " value="<?php echo (isset($pin_code) && !empty($pin_code)) ? $pin_code : ''; ?>" pattern="[0-9]{6}" title="Only 6 digits are allowed">
            <label>Pincode</label>
         </p>
         <p>
            <input type="text" name="mobile_number" placeholder=" " value="<?php echo (isset($mobile_number) && !empty($mobile_number)) ? $mobile_number : ''; ?>" pattern="[0-9]{10}" title="Only 10 digits are allowed">
            <label>Mobile Number</label>
         </p>
         <p>
            <input type="text" name="land_number" placeholder=" " value="<?php echo (isset($land_number) && !empty($land_number)) ? $land_number : ''; ?>" pattern="[0-9]+" title="Only digits are allowed">
            <label>Land Number</label>
         </p>
         <p>
            <input type="email" name="email_id" placeholder=" " value="<?php echo (isset($emai_id) && !empty($email_id)) ? $email_id : ''; ?>">
            <label>Email</label>
         </p>
         <div style="text-align: center;">
            <button type="submit" name="update" class="button blue"><i class="material-icons">update</i>Update</button>
         </div>            
      </form>
   </div>
</body>

</html>
<?php
if (isset($_POST['update'])) {
   $application_number = $_POST['application_number'];
   $application_subject = $_POST['application_subject'];
   $name = $_POST['name'];
   $mobile_number = $_POST['mobile_number'];
   $address1 = $_POST['address1'];
   $address2 = $_POST['address2'];
   $address3 = $_POST['address3'];
   $post_office = $_POST['post_office'];
   $pin_code = intval($_POST['pin_code']);
   $village = $_POST['village'];
   $email_id = $_POST['email_id'];
   $land_number = $_POST['land_number'];
   $query1 = "UPDATE application SET application_subject='$application_subject' WHERE application_number='$application_number';";
   $result1 = mysqli_query($conn, $query1);
   $query2 = "UPDATE person SET name='$name',mobile_number='$mobile_number',address1='$address1',address2='$address2',address3='$address3',post_office='$post_office',pin_code=$pin_code,village='$village',email_id='$email_id',land_number='$land_number' WHERE person_id=(SELECT person_id FROM application WHERE application_number='$application_number');";
   $result2 = mysqli_query($conn, $query2);
   if ($result1 || $result2) {
?>
      <script>
         alert("Data Updated");
         location.replace("Admin_Application_Search.php");
      </script>
   <?php
      //header('location:Admin_Application_Search.php'); 
   } else {
   ?>
      <script>
         alert("Failed To Update");
      </script>
<?php
   }
}
?>