<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
?>
<!DOCTYPE html>
<html>

<head>
	<title>Update Application Status</title>
	<link rel="stylesheet" href="../form.css">
	<style>
		.wrapper {
			max-width: 70%;
			min-height: 300px;
		}
		input[type=text] {
			width: 80%;
		}

		.btns {
			padding: 0 15%;
		}

		.error {
			margin-top: 16px;
		}
	</style>
</head>

<body>

	<div class="wrapper">
		<div class="heading">UPDATE STATUS</div>
		<form method="post">
			<div class="btns">
				<label><b>Search By</b></label>
				<label>
					<input name="searchby" type="radio" value="application_number" id="searchby"><b>Application Number</b>
				</label>
				<label>
					<input name="searchby" type="radio" value="file_number" id="searchby"><b>File Number</b>
				</label>
			</div>
			<div class="btns">
				<input name="number" type="text">
				<button name="search" value="search" class="button blue"><i class="material-icons">search</i>Search</button>
			</div>
		
	<?php
	if (isset($_POST['search'])) {
		$value = isset($_POST['searchby'])?$_POST['searchby']:'';
		$number = $_POST['number']!=''?$_POST['number']:'invalid';

		if ($value == "application_number") {
			$sql = mysqli_query($conn, "select application_number from application where application_number ='$number'");
			if (mysqli_num_rows($sql) > 0) {
				while ($row = mysqli_fetch_array($sql)) {
					$ap_no = $row['application_number'];
					$_SESSION['application_number'] = $ap_no;
				}
	?>
				<script>
					location.replace('Status_Change.php');
				</script>
			<?php
			} 
			else {
			?>
				<div class="error"><?php echo "Application number is incorrect or doesn't exits"; ?></div>
			<?php
			}
		} 
		else if ($value == "file_number") {
			$qry = mysqli_query($conn, "select application_number from application where file_number ='$number'");
			if (mysqli_num_rows($qry) > 0) {
				while ($row = mysqli_fetch_array($qry)) {
					$ap_no = $row['application_number'];
					$_SESSION['application_number'] = $ap_no;
				}
				?>
				<script>
					location.replace('Status_Change.php');
				</script>
			<?php
			} 
			else {
			?>
				<div class="error"><?php echo "File number is incorrect or doesn't exits"; ?></div>
			<?php
				}
		} 
		else {
			?>
			<div class="error"><?php echo "Please select Application number or File number"; ?></div>
			<?php
		}
	}

					?>
		</form>
	</div>
</body>

</html>