<?php 
session_start();
if(!isset($_SESSION['taluk_id']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php" ;
	$taluk_id=$_SESSION['taluk_id'];
	$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
	$row = $result->fetch_assoc();
	$taluk = $row['taluk_name'];
	include "../header.php";
	include "../Footer.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Update File Number</title>
	<link rel="stylesheet" type="text/css" href="../form.css">
	<style type="text/css">
		.wrapper {
			max-width: 80%;
			min-height: 300px;
		}

		input[type=date] {
			width: 80%;
		}

		.btns {
			padding: 0 20%;
		}
	</style>
</head>
<body>
	
	<div class="wrapper">
		<div class="heading">UPDATE FILE NUMBER</div>
		<form method="POST">
			<div class="btns">
				<input type="date" name="date" id="date" value="<?php echo date('Y-m-d',time()) ?>">
				<button type="submit" name="search" id="search" class="button blue"><i class="material-icons">search</i>Search</button>
			</div>
		</form>
		<div id="list">
			<table class="table">
				<?php
					
				$date=date("Y-m-d");
				$result=$conn->query("SELECT * FROM application WHERE date_applied LIKE '$date%' AND taluk_id=$taluk_id;");
				if($result->num_rows>0)
				{
					?>
				<tr style="text-align: left">
					<th>Application Number</th>
					<th>Applicant Name</th>
					<th>Application Purpose</th>
					<th>File Number</th>
					<th></th>
				</tr>
					<?php
					while($row=$result->fetch_assoc()) 
					{			
						$application_no=$row['application_number'];
						$person_id=$row['person_id'];
						$purpose=$row['application_subject'];
						$file_no=$row['file_number'];
						$result1=$conn->query("SELECT name FROM person WHERE person_id='$person_id';");
						if($result1->num_rows>0)
						{
							if($row1=$result1->fetch_assoc()) 
							{
								$name=$row1['name'];
							}
						}
						?>
						<tr>
							<td><?php echo $application_no; ?></td>
							<td><?php echo $name; ?></td>
							<td><?php echo $purpose; ?></td>
							<td><?php echo $file_no; ?></td>
							<td><a href="Assign_FileNumber.php?apl_no=<?php echo $application_no; ?>" class="a"><i class="material-icons">edit</i>Update File Number</a></td>
						</tr>
						<?php
					}
				}
				else {
					echo "<div class='text'>Nothing to display</div>";
				}
					?>
				</table>		
			</form>
		</div>
	</div>
</body>
</html>
<?php
    
    if (isset($_REQUEST['search'])) 
    {
    	$date=date('Y-m-d',strtotime($_REQUEST['date']));
    	$result=$conn->query("SELECT * FROM application WHERE date_applied LIKE '$date%' AND taluk_id=$taluk_id;");
    	if ($result->num_rows>0) 
    	{
		?>
			<script type="text/javascript">
				var content='<table class="table"><tr style="text-align: left"><th>Application Number</th><th>Applicant Name</th><th>Application Purpose</th><th>File Number</th><th></th></tr>';
			</script>
    	<?php
    		while($row=$result->fetch_assoc()) 
			{
				
				$application_no=$row['application_number'];
				$person_id=$row['person_id'];
				$purpose=$row['application_subject'];
				$file_no=$row['file_number'];
				$result1=$conn->query("SELECT name FROM person WHERE person_id='$person_id';");
				if($result1->num_rows>0)
				{
				    if($row1=$result1->fetch_assoc()) 
                    {
                        $name=$row1['name'];
                    }
				}
			?>
			    <script type="text/javascript">
				    content+='<tr><td><?php echo $application_no; ?></td><td><?php echo $name; ?></td><td><?php echo $purpose; ?></td><td><?php echo $file_no; ?></td><td><a href="Assign_FileNumber.php?apl_no=<?php echo $application_no; ?>" class="a"><i class="material-icons">edit</i>Update File Number</a</td></tr>';
			    </script>
			<?php
			}	            
    	}
    	?>
    	<script type="text/javascript">
    		content+="</table>";
			document.getElementById('list').innerHTML=content;
    		document.getElementById('date').value="<?php echo $date ?>";
    	</script>
    <?php	
    }
?>


