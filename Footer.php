<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    .footer {
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
      box-sizing: border-box;
      font-family: 'Poppins', sans-serif;
      width: 100%;
      max-height: 40px;
      overflow: hidden;
      background-color: #03A9F4;
      color: white;
      text-align: center;
      z-index: 1;
    }
  </style>
</head>

<body>

  <div class="footer">
    <p>Developed, hosted and maintained by Dept. of Computer Applications, RIT Kottayam.</p>
  </div>

</body>

</html>