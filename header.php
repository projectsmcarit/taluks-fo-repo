<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @import url("https://fonts.googleapis.com/icon?family=Material+Icons");
        * {
            box-sizing: border-box;
            font-family: 'Poppins', sans-serif;
        }

        .material-icons {
            vertical-align: -15%;
            font-size: 18px;
        }

        body {
            background: #ecf0f3
        }

        .logo {
            width: 120px;
            margin: auto
        }

        .logo img {
            width: 100%;
            height: 80px;
            object-fit: cover;
            border-radius: 30%;
            box-shadow: 0px 0px 3px #5f5f5f, 0px 0px 0px 5px #ecf0f3, 8px 8px 15px #a7aaa7, -8px -8px 15px #fff
        }

        .name {
            font-weight: 600;
            font-size: 2rem;
            letter-spacing: 1.3px;
            padding: 15px;
            text-align: center;
            color: #555
        }

        .heading {
            font-weight: 600;
            font-size: 1.5rem;
            letter-spacing: 1.3px;
            padding: 16px;
            text-align: center;
            color: #555
        }

        .navbar {
            width: 100%;
            max-height: 60px;
            overflow: hidden;
            padding: 5px;
            background-color: #03A9F4;
            border-radius: 15px;
            box-shadow: 13px 13px 20px #cbced1, -13px -13px 20px #fff
        }

        .navbar a {
            float: left;
            font-size: 16px;
            color: #fff;
            text-align: center;
            padding: 16px;
            text-decoration: none;
        }

        .dropdown {
            float: left;
            overflow: hidden;
        }

        .dropdown .dropbtn {
            font-size: 16px;
            color: #fff;
            border: none;
            outline: none;
            padding: 16px;
            background-color: inherit;
            font-family: inherit;
            margin: 0;
            /* Important for vertical align on mobile phones */
        }

        .navbar a:hover,
        .dropdown:hover .dropbtn {
            border-radius: 15px;
            box-shadow: inset 5px 5px 5px #047bb3, inset -5px -5px 5px #44bef7;
            transition: all 0.2s ease-in-out;
            appearance: none;
            -webkit-appearance: none;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #03A9F4;
            padding: 5px;
            min-width: 100px;
            border-radius: 15px;
            box-shadow: 5px 5px 10px #BABECC;
            z-index: 1;
        }

        .dropdown-content a {
            float: none;
            padding: 16px;
            text-decoration: none;
            display: block;
            text-align: left;
        }

        .dropdown-content a:hover {
            border-radius: 15px;
            box-shadow: inset 5px 5px 5px #047bb3, inset -5px -5px 5px #44bef7;
            transition: all 0.2s ease-in-out;
            appearance: none;
            -webkit-appearance: none;
        }

        /* Show the dropdown menu on hover */
        .dropdown:hover .dropdown-content {
            display: block;
        }
    </style>
</head>

<body>
    <div class="logo"> <img src="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/gvt.jpg" alt=""> </div>
    <div class="text-center mt-4 name"><?php echo $taluk; ?> TALUK FRONT OFFICE</div>
    <div class="navbar">
        <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Admin_Home.php"><i class="material-icons">home</i></a>
        <div class="dropdown">
            <button class="dropbtn">Application
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content">
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Admin_Application_New/Admin_Application_New_Form.php">New Application</a>
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Admin_Application_New/View_Application.php">View Applications</a>
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Application/Admin_Application_Search.php">Edit Application</a>
            </div>
        </div>
        <div class="dropdown">
            <button class="dropbtn">Update Application
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content">
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Application/Application_Update_FileNumber.php">Update File Number</a>
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Application/Application_Update_Status.php">Update Status</a>
            </div>
        </div>
        <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Admin_Pass/Admin_Pass_Generation.php">Issue Pass</a>
        <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Admin_Pass/Visitors_List.php">Visitors List</a>
        <div class="dropdown">
            <button class="dropbtn">Enquiry
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content">
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Enquiry/Enquiry_New.php">New Enquiry</a>
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Enquiry/Enquiry_List.php">Enquiry List</a>
            </div>
        </div>
        <div class="dropdown">
            <button class="dropbtn">Section
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content">
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Section/Admin_Section_View.php">View Section</a>
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Section/Admin_Section_Add.php">New Section</a>
            </div>
        </div>
        <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/Logout.php" style="float:right"><i class="material-icons">logout</i></a>
        <div class="dropdown" style="float:right">
            <button class="dropbtn"><i class="material-icons">manage_accounts</i>
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content">
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/edit_profile.php">Edit Profile</a>
                <a href="http://117.232.108.35:8086/www/kertfro/taluks-fo-repo/change_pswrd.php">Change Password</a>
            </div>
        </div>        
    </div>
</body>

</html>