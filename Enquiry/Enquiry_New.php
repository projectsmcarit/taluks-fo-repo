<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
   echo "<script>alert('Session Expired');</script>";
   echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
   <title>New Equiry</title>
   <!--styles file including-->
   <link rel="stylesheet" href="../form1.css">
</head>

<body>
   <!--form to add new section-->
   <div class="wrapper">
      <div class="text-center mt-4 name">NEW ENQUIRY</div>
      <form class="p-3 mt-3" method="post">
         <p>
            <input type="text" placeholder=" " id="name" name="name" required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
            <label>Name*</label>
         </p>
         <p>
            <input type="text" placeholder=" " id="phone" name="phone" value="" pattern="[0-9]{10}" required>
            <label>Phone Number*</label>
         </p>
         <p>
            <input type="text" placeholder=" " id="village" name="village">
            <label>Village</label>
         </p>
         <p>
            <input type="text" placeholder=" " id="file_no" name="file_no" required>
            <label>File Number*</label>
         </p>
         <p>
            <input type="text" placeholder=" " id="application_no" name="application_no" value="">
            <label>Application Number</label>
         </p>
         <p>
            <input type="text" placeholder=" " list="sub" name="subject" id="subject">
            <label>Application Subject</label>
            <datalist id="sub">
               <option value="Area Alteration">
               <option value="Arms, Explosive/ Mining">
               <option value="Auction">
               <option value="Birth/ Death Registration">
               <option value="Census">
               <option value="Certificate">
               <option value="Chief Ministers DRF">
               <option value="Compassionate Employment">
               <option value="Covid-19">
               <option value="Disaster Management">
               <option value="Election">
               <option value="Electricity">
               <option value="Encroachment of Land">
               <option value="Establishment">
               <option value="Eviction">
               <option value="Irrigation">
               <option value="KLU (Land Utilization)">
               <option value="Land Assignment">
               <option value="Land Conversion">
               <option value="Land Transfer">
               <option value="Law and Order">
               <option value="Legal Heirship Certificate">
               <option value="Luxury Tax">
               <option value="Miscellanious">
               <option value="NFBS">
               <option value="Paddy and Wetland Act">
               <option value="Pattayam">
               <option value="Pensions">
               <option value="Point Out of Boundary">
               <option value="Pumping Subsidy">
               <option value="PV">
               <option value="PV Appeal">
               <option value="PV Cancellation">
               <option value="Resurvey Correction">
               <option value="Revenue Recovery">
               <option value="Right to Information Act">
               <option value="Right to Service">
               <option value="Surplus Land">
               <option value="Tree Cutting">
            </datalist>
         </p>
         <p>
            <?php
            $sql = "SELECT section_id, section_name from section where for_application='Y' AND taluk_id=$taluk_id order by section_name;";
            $result = $conn->query($sql);
            ?>
            <select id="section" name="section">
            <label>Section</label>   
               <option value="" selected>Section</option>
               <?php
               if ($result->num_rows > 0)
                  while ($row = $result->fetch_assoc())
                     echo '<option value="' . $row['section_id'] . '">' . $row['section_name'] . '</option>';
               ?>
            </select>
         </p>
         <p>
            <input type="text" placeholder=" " id="date_of_application" name="date_of_application" value="" onfocus="(this.type='date')" onblur="(this.type='text')">
            <label>Date of Application</label>
         </p>
         <div class="btns">
            <button type="reset" class="button"><i class="material-icons">restart_alt</i>Reset</button>
            <button class="blue button" type="submit" name="add"><i class="material-icons">done</i>Submit</button>
         </div>
      </form>
   </div>
</body>

</html>
<?php
if (isset($_POST['add'])) {
   $name = $_POST['name'];
   $phone = $_POST['phone'];
   $village = $_POST['village'];
   $subject = $_POST['subject'];
   $section = $_POST['section'];
   $application_no = $_POST['application_no'];
   $file_no = $_POST['file_no'];
   $date_of_application = $_POST['date_of_application'];
   date_default_timezone_set("Asia/Kolkata");
   $date = date("Y-m-d h:i:s", time());

   //insert new section to table enquiry
   if ($section == "")
      $sql = "INSERT INTO enquiry(name, phone_number, village, subject, application_number, file_number, date_application, status, date_enquiry, taluk_id)values('$name','$phone','$village','$subject','$application_no','$file_no','$date_of_application','Pending', '$date', $taluk_id);";
   else
      $sql = "INSERT INTO enquiry(name, phone_number, village, subject, section_id, application_number, file_number, date_application, status, date_enquiry, taluk_id)values('$name','$phone','$village','$subject',$section,'$application_no','$file_no','$date_of_application','Pending', '$date', $taluk_id);";
   if ($conn->query($sql)) {
?>
      <script>
         alert(" Added Successfully");
         location.replace("Enquiry_List.php");
      </script>
   <?php
   } else {
   ?>
      <script>
         alert("failed");
      </script>
<?php
   }
}
?>