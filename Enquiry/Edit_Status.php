<?php
session_start();

$enquiry_no = $_GET['enquiry_no'];
if (!isset($_SESSION['taluk_id'])) {
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">location.replace("../index.php");</script>';
}

//Including database connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
$query = "SELECT * FROM enquiry WHERE enquiry_number='$enquiry_no'";
$result = mysqli_query($conn, $query);
while ($row = mysqli_fetch_assoc($result)) {
	$file_number = $row['file_number'];
	$name = $row['name'];
	$status = $row['status'];
	$date = $row['date_enquiry'];
	$phone_number = $row['phone_number'];
	$application_no = $row['application_number'];
?>
	<!DOCTYPE html>
	<html>

	<head>
		<title>Update Status</title>
		<link rel="stylesheet" href="../form1.css">
	</head>

	<body>
		<div class="wrapper">
			<div class="text-center mt-4 name">UPDATE STATUS</div>
			<form class="p-3 mt-3" method="post" onSubmit="return confirm('Are you sure about the updation?')">
				<p>
					<input type="text" placeholder=" "  name="file_no" disabled="disabled" value="<?php echo $file_number; ?>">
					<label >File Number</label>
				</p>
				<p>
					<input name="name" placeholder=" " type="text" disabled="disabled" value="<?php echo $name; ?>">
					<label>Name</label>
				</p>
				<p>
					<input type="text" placeholder=" " list="status1" name="status" id="status" required value="<?php echo $status; ?>">
					<label>Status*</label>
					<datalist id="status1">
						<option value="is awaiting Surveyor report">
						<option value="is awaiting V.O report">
						<option value="is disposed">
						<option value="is ordered">
						<option value="is processing">
						<option value="is with Head Surveyor">
						<option value="is with JS">
						<option value="is with Other Office">
						<option value="is with Tahsildar">
						<option value="is with Village Office">
						<option value="needed permission of DC">
						<option value="required additional document">
						<option value="required site inspection">
					</datalist>
				</p>
				<div class="btns">
					<a href="Enquiry_List.php?date=<?php echo $date ?>&status=<?php echo $status=='Pending'?'pending':'attended'; ?>&search=search"><button type="button" class="button"><i class="material-icons">close</i>Cancel</button></a>
					<button name="update" type="submit" class="button blue"><i class="material-icons">update</i>Update</button>
				</div>
			</form>
		</div>
	</body>

	</html>
	<?php
	if (isset($_POST['update'])) {
		$status = $_POST['status'];
		if ($conn->query("UPDATE enquiry SET status='$status' WHERE enquiry_number='$enquiry_no'")) {
			$application_no = 0;
			if (strlen($phone_number) == 10) {
				$message = $phone_number . ',Dear ' . $name . ', your application with file number ' . $file_no . ' ' . $status . '.';
				$array = array($phone_number, $message);
				date_default_timezone_set("Asia/Kolkata");
				$filename = date('Ymdhis', time()) . '.csv';
				$file = fopen($taluk . '/' . $filename, 'w') or die("Unable to open file!");
				fwrite($file, $message);
				fclose($file);
			}
			$result1 = $conn->query("UPDATE application SET status='$status' WHERE application_number='$application_no'");
		
	?>
			<script>
				alert("Updated Successfully");
				location.replace("Enquiry_List.php?date=<?php echo $date ?>&status=<?php echo $status=='Pending'?'pending':'attended'; ?>&search=search");
			</script>
		<?php
		} else {
		?>
			<script>
				alert("Failed");
			</script>
	<?php
		}
	}
}
	?>