<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">location.replace("../index.php");</script>';
}
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>

<head>
	<title>Enquiry List</title>
	<link rel="stylesheet" href="../form.css">
	<style type="text/css">
		.wrapper {
			max-width: 95%;
			min-height: 300px;
		}

		.date {
			margin: 0;
		}

		input[type=date] {
			width: 82%;
		}

		.btns {
			padding: 0 22%;
		}

	</style>
</head>

<body>
	<div class="wrapper">
		<div id="printdiv" hidden></div>
		<div class="heading">ENQUIRY LIST</div>
		<form method="POST">
			<div class="btns">
				<label>
					<input type="radio" name="status" id="pending" value="pending" checked>
					<b>Pending</b>
				</label>
				<label>
					<input type="radio" name="status" id="attended" value="attended">
					<b>Attended</b>
				</label>
			</div>
			<div class="btns">
				<input type="date" name="date" id="date" value="<?php echo date('Y-m-d', time()) ?>" />
				<button type="submit" name="search" id="search" class="blue button"><i class="material-icons">search</i>Search</button>
			</div>
		</form>

		<div id="list">
			<?php
			$date = date("Y-m-d", time());
			?>
			<script type="text/javascript">
				var printvalue = '<h2 align="center">Pending Enquiry List on <?php echo date("d/m/Y", strtotime($date)); ?><h2><table border="1" width="100%"><tr style="text-align: left"><th>File Number</th><th>Name</th><th>Application Subject</th><th>Section</th><th>Status</th></tr>';
			</script>
			<?php
			$result = $conn->query("SELECT * FROM enquiry WHERE date_enquiry LIKE '$date%' AND status='Pending' AND taluk_id=$taluk_id");
			if ($result->num_rows > 0) {
			?>
				<table class="table">
					<tr>
						<th>File Number</th>
						<th>Name</th>
						<th>Phone Number</th>
						<th>Application Subject</th>
						<th>Section</th>
						<th>Status</th>
						<th></th>
						<th></th>
					</tr>
					<?php
					while ($row = $result->fetch_assoc()) {
						$enquiry_no = $row['enquiry_number'];
						$file_no = $row['file_number'];
						$name = $row['name'];
						$phone_no = $row['phone_number'];
						$subject = $row['subject'];
						$section_id = $row['section_id'];
						$status = $row['status'];
						$section = "";
						if ($res = $conn->query("SELECT section_name from section where section_id='$section_id' AND taluk_id=$taluk_id"))
							if ($row1 = $res->fetch_array())
								$section = $row1['section_name'];
					?>
						<tr>
							<td><?php echo $file_no; ?></td>
							<td><?php echo $name; ?></td>
							<td><?php echo $phone_no; ?></td>
							<td><?php echo $subject; ?></td>
							<td><?php echo $section; ?></td>
							<td><?php echo $status; ?></td>
							<td><a class="a" href="Enquiry_Details.php?enquiry_no=<?php echo $enquiry_no; ?>"><i class="material-icons">table_view</i>View Details</a></td>
							<td><a href="Edit_Status.php?enquiry_no=<?php echo $enquiry_no; ?>" class="a"><i class="material-icons">edit</i>Update Status</a></td>
						</tr>
						<script type="text/javascript">
							printvalue += '<tr><td><?php echo $file_no; ?></td><td><?php echo $name; ?></td><td><?php echo $subject; ?></td><td><?php echo $section; ?></td><td><input type="text" style="border:none"></td></tr>';
						</script>
					<?php
					}
					?>
				</table>
				<script type="text/javascript">
					printvalue += '</table>';
					document.getElementById('printdiv').innerHTML = printvalue;
				</script>
				<div style="text-align: center;">
					<button type="button" id="print" class="button blue" onclick="printDiv('printdiv')"><i class="material-icons">print</i>Print</button>
				</div>
			<?php
			} else {
				echo "<div class='text'>Nothing to display</div>";
			}
			?>
		</div>
	</div>
	<br>
</body>

</html>

<script type="text/javascript">
	function printDiv(divId) {
		let mywindow = window.open('', 'PRINT', 'height=650,width=900,top=100,left=150');

		mywindow.document.write('<html><head><title>Print</title>');
		mywindow.document.write('</head><body>');
		mywindow.document.write(document.getElementById(divId).innerHTML);
		mywindow.document.write('</body></html>');

		mywindow.document.close();
		mywindow.focus();

		mywindow.print();
		mywindow.close();

		return true;
	}
</script>

<?php
if (isset($_REQUEST['search'])) {
	$date = date('Y-m-d', strtotime($_REQUEST['date']));
	$status1 = $_REQUEST['status'];

?>
	<script>
		document.getElementById('date').value = "<?php echo $date ?>";
		document.getElementById('<?php echo $status1 ?>').checked = true;
	</script>
	<?php
	if ($status1 == 'pending')
		$result = $conn->query("SELECT * FROM enquiry WHERE date_enquiry LIKE '$date%' AND status='Pending' AND taluk_id=$taluk_id;");
	else
		$result = $conn->query("SELECT * FROM enquiry WHERE date_enquiry LIKE '$date%' AND status<>'Pending' AND taluk_id=$taluk_id;");

	if ($result->num_rows > 0) {
	?>
		<script type="text/javascript">
			var content = '<table class="table"><tr><th>File Number</th><th>Name</th><th>Phone Number</th><th>Application Subject</th><th>Section</th><th>Status</th><th></th><th></th></tr>';
			var printvalue1 = '<h2 align="center">Pending Enquiry List on <?php echo date("d/m/Y", strtotime($date)); ?><h2><table border="1" width="100%"><tr style="text-align: left"><th>File Number</th><th>Name</th><th>Application Subject</th><th>Section</th><th>Status</th></tr>';
		</script>
		<?php
		while ($row = $result->fetch_assoc()) {
			$enquiry_no = $row['enquiry_number'];
			$file_no = $row['file_number'];
			$name = $row['name'];
			$phone_no = $row['phone_number'];
			$subject = $row['subject'];
			$section_id = $row['section_id'];
			$status = $row['status'];
			$section = "";
			if ($res = $conn->query("SELECT section_name from section where section_id='$section_id'"))
				if ($row1 = $res->fetch_array())
					$section = $row1['section_name'];
		?>
			<script type="text/javascript">
				content += '<tr><td><?php echo $file_no; ?></td><td><?php echo $name; ?></td><td><?php echo $phone_no; ?></td><td><?php echo $subject; ?></td><td><?php echo $section; ?></td><td><?php echo $status; ?></td><td><a class="a" href="Enquiry_Details.php?enquiry_no=<?php echo $enquiry_no; ?>"><i class="material-icons">table_view</i>View Details</a></td><td><a class="a" href="Edit_Status.php?enquiry_no=<?php echo $enquiry_no; ?>"><i class="material-icons">edit</i>Update Status</a></td></tr>';
				printvalue1 += '<tr><td><?php echo $file_no; ?></td><td><?php echo $name; ?></td><td><?php echo $subject; ?></td><td><?php echo $section; ?></td><td><input type="text" style="border:none"></td></tr>';
			</script>
		<?php
		}
		?>
		<script>
			content += "</table>";
			printvalue1 += '</table>';
		</script>
		<?php
		if ($status1 == 'pending') {
		?>
			<script type="text/javascript">
				content += '<div style="text-align: center;"><button type="button" id="print" class="button blue" onclick="printDiv(';
				content += "'printdiv'";
				content += ')"><i class="material-icons">print</i>Print</button>';
			</script>
		<?php
		}
	} else {
		?>
		<script>
			content = "<div class='text'>Nothing to display</div>";
		</script>
	<?php
	}
	?>
	<script type="text/javascript">
		document.getElementById('list').innerHTML = content;
		document.getElementById('printdiv').innerHTML = printvalue1;
	</script>
<?php
}

?>