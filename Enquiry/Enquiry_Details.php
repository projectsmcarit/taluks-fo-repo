<?php
session_start();

$enquiry_no = $_GET['enquiry_no'];
if (!isset($_SESSION['taluk_id'])) {
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">location.replace("../index.php");</script>';
}

//Including database connection file
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
$query = "SELECT * FROM enquiry WHERE enquiry_number='$enquiry_no'";
$result = mysqli_query($conn, $query);
while ($row = mysqli_fetch_assoc($result)) {

	$name = $row['name'];
	$phn_no = $row['phone_number'];
	$village = $row['village'];
	$file_no = $row['file_number'];
	$app_no=$row['application_number'];
	$subject = $row['subject'];
	$section_id = $row['section_id'];
	$app_date = $row['date_application'];
	$date = $row['date_enquiry'];
	$status = $row['status'] == 'Pending' ? 'pending' : 'attended';
?>
	<!DOCTYPE html>
	<html>

	<head>
		<title>Enquiry Details</title>
		<link rel="stylesheet" href="../form1.css">
		<script>
			function show() {
				alert('You can now edit the Enquiry');
				document.title='Edit Enquiry';
				document.getElementById('heading').innerHTML='EDIT ENQUIRY';
				document.getElementById('one').style.display='none';
				document.getElementById('two').style.display='flex';
				document.getElementById('file_no').removeAttribute('disabled');
				document.getElementById('name').removeAttribute('disabled');
				document.getElementById('phone').removeAttribute('disabled');
				document.getElementById('village').removeAttribute('disabled');
				document.getElementById('application_no').removeAttribute('disabled');
				document.getElementById('subject').removeAttribute('disabled');
				document.getElementById('section').removeAttribute('disabled');
				document.getElementById('date_of_application').removeAttribute('disabled');
			}
		</script>
	</head>

	<body>
		<div class="wrapper">			
			<div id="heading" class="text-center mt-4 name">ENQUIRY DETAILS</div>
			<form class="p-3 mt-3" method="post">
				<p>
					<input type="text" placeholder=" " value="<?php echo $file_no; ?>" id="file_no" name="file_no" required disabled>
					<label>File Number*</label>
				</p>
				<p>
					<input type="text" placeholder=" " value="<?php echo $name; ?>" id="name" name="name" required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable" disabled>
					<label>Name*</label>
				</p>
				<p>
					<input type="text" placeholder=" " value="<?php echo $phn_no; ?>" id="phone" name="phone" pattern="[0-9]{10}" required disabled>
					<label>Phone Number*</label>
				</p>
				<p>
					<input type="text" placeholder=" " value="<?php echo $village; ?>" id="village" name="village" disabled>
					<label>Village</label>
				</p>
				<p>
					<input type="text" placeholder=" " value="<?php echo $app_no; ?>" id="application_no" name="application_no" disabled>
					<label>Appication Number</label>
				</p>
				<p>
					<input type="text" placeholder=" " list="sub" name="subject" id="subject" value="<?php echo $subject; ?>" disabled>
					<label>Application Subject</label>
					<datalist id="sub">
						<option value="Area Alteration">
						<option value="Arms, Explosive/ Mining">
						<option value="Auction">
						<option value="Birth/ Death Registration">
						<option value="Census">
						<option value="Certificate">
						<option value="Chief Ministers DRF">
						<option value="Compassionate Employment">
						<option value="Covid-19">
						<option value="Disaster Management">
						<option value="Election">
						<option value="Electricity">
						<option value="Encroachment of Land">
						<option value="Establishment">
						<option value="Eviction">
						<option value="Irrigation">
						<option value="KLU (Land Utilization)">
						<option value="Land Assignment">
						<option value="Land Conversion">
						<option value="Land Transfer">
						<option value="Law and Order">
						<option value="Legal Heirship Certificate">
						<option value="Luxury Tax">
						<option value="Miscellanious">
						<option value="NFBS">
						<option value="Paddy and Wetland Act">
						<option value="Pattayam">
						<option value="Pensions">
						<option value="Point Out of Boundary">
						<option value="Pumping Subsidy">
						<option value="PV">
						<option value="PV Appeal">
						<option value="PV Cancellation">
						<option value="Resurvey Correction">
						<option value="Revenue Recovery">
						<option value="Right to Information Act">
						<option value="Right to Service">
						<option value="Surplus Land">
						<option value="Tree Cutting">
					</datalist>
				</p>
				<p>
					<?php
					$sql = "SELECT section_id, section_name from section where for_application='Y' AND taluk_id=$taluk_id order by section_name;";
					$result = $conn->query($sql);
					?>
					<select id="section" name="section" disabled>
						<option value="" selected></option>
						<?php
						if ($result->num_rows > 0)
							while ($row = $result->fetch_assoc())
								if($row['section_id']==$section_id) 
									echo '<option value="' . $row['section_id'] . '" selected>' . $row['section_name'] . '</option>';
								else
									echo '<option value="' . $row['section_id'] . '">' . $row['section_name'] . '</option>';
						?>
					</select>
					<label>Section</label>
				</p>
				<p>
					<input type="text" placeholder=" " value="<?php echo $app_date!='0000-00-00 00:00:00'?date('d-m-Y', strtotime($app_date)):""; ?>" id="date_of_application" name="date_of_application" value="" onfocus="(this.type='date')" onblur="(this.type='text')" disabled>
					<label>Date of Application</label>
				</p>
				<div class="btns" id="one">
					<a href="Enquiry_List.php?date=<?php echo $date ?>&status=<?php echo $status ?>&search=search"><button type="button" id="back" class="button"><i class="material-icons">arrow_back</i>Back</button></a>
					<button type="button" id="edit" class="button blue" onclick="show()"><i class="material-icons">edit</i>Edit</button>
				</div>
				<div class="btns" id="two" style="display: none;">
					<a href="Enquiry_List.php?date=<?php echo $date ?>&status=<?php echo $status ?>&search=search"><button type="button" class="button"><i class="material-icons">close</i>Cancel</button></a>
					<button name="submit" type="submit" class="button blue"><i class="material-icons">done</i>Submit</button>
				</div>
			</form>
		</div>
	</body>

	</html>
<?php
if (isset($_POST['submit'])) {
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$village = $_POST['village'];
	$subject = $_POST['subject'];
	$section = $_POST['section'];
	$application_no = $_POST['application_no'];
	$file_no = $_POST['file_no'];
	$date_of_application = $_POST['date_of_application'];
	date_default_timezone_set("Asia/Kolkata");
 
	//insert new section to table enquiry
	if ($section == "")
	   $sql = "UPDATE enquiry SET name='$name', phone_number='$phone', village='$village', subject='$subject', application_number='$application_no', file_number='$file_no', date_application='$date_of_application' WHERE enquiry_number='$enquiry_no';";
	else
	   $sql = "UPDATE enquiry SET name='$name', phone_number='$phone', village='$village', subject='$subject', section_id=$section, application_number='$application_no', file_number='$file_no', date_application='$date_of_application' WHERE enquiry_number='$enquiry_no';";
	if ($conn->query($sql)) {
 ?>
	   <script>
		  alert("Edited Successfully");
		  location.replace("Enquiry_List.php?date=<?php echo $date ?>&status=<?php echo $status ?>&search=search");
	   </script>
	<?php
	} else {
	?>
	   <script>
		  alert("failed");
	   </script>
 <?php
	}
 }
}
?>