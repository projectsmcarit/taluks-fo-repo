<?php
session_start();
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;
$taluk_id=$_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];

$sql1='SELECT * FROM pass ORDER BY CAST(pass_id as int) DESC LIMIT 1';
	if($result1=$conn->query($sql1))
    {
        if($row1=$result1->fetch_assoc())
        {
            $token_no=$row1['token_number'];
            $name=$row1['name'];
            $locality=$row1['locality'];
            $phone_number=$row1['phone_number'];
            $visit_purpose=$row1['visit_purpose'];
            $section_id=$row1['section_id'];
            $section_name="";
			$date=date('d/m/Y',strtotime($row1['date_of_pass']));
			$time=date('h:i a',strtotime($row1['date_of_pass']));
            $sql2="SELECT section_name FROM section WHERE section_id='$section_id';";
            if($result2=$conn->query($sql2))
            {
                if($row2=$result2->fetch_assoc())
                {
                    $section_name=$row2['section_name'];
                }
            }

        }
    }
 function generatePDF($source, $output, $image, $taluk, $token_no, $name, $locality, $phone_number, $section, $visit_purpose, $date, $time)
 {
$pdf = new FPDI('Landscape','mm',array(210,148)); // Array sets the X, Y dimensions in mm
$pdf->AddPage();
$pagecount = $pdf->setSourceFile($source);
$tppl = $pdf->importPage(1);

$pdf->useTemplate($tppl, 0, 0, 210, 148);

$pdf->Image($image,163,33.53,17.15,17.15); // X start, Y start, X width, Y width in mm

$pdf->SetFont('Times','B',20); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(40,28.5); // X start, Y start in mm
$pdf->Write(0, $token_no);

$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(35.5,55.5); // X start, Y start in mm
$pdf->Write(0, $name);

$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(35.5,63); // X start, Y start in mm
$pdf->Write(0, $locality);


$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(35.5,70.5); // X start, Y start in mm
$pdf->Write(0, $phone_number);


$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(75.5,78); // X start, Y start in mm
$pdf->Write(0, $section);


$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(54,85); // X start, Y start in mm
$pdf->Write(0, $visit_purpose);


$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(25.15,92.2); // X start, Y start in mm
$pdf->Write(0, $date);

$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(17.5,119.5); // X start, Y start in mm
$pdf->Write(0, $date);

$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0); // RGB
$pdf->SetXY(65.5,92.2); // X start, Y start in mm
$pdf->Write(0, $time);

$pdf->Output($output, "I");
}
generatePDF("Pass.pdf", "output.pdf", "../gvt.jpg", $taluk, $token_no, $name, $locality, 'Phn.no: '.$phone_number, $section_name, $visit_purpose, $date, $time);
?>
