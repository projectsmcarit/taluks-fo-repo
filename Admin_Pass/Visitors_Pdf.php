<?php
session_start();
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;
$date=$_GET['date'];
$taluk_id=$_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
$pdf=new FPDI();
$pdf->AddPage();
$pdf->SetFont('Times','B',12); // Font Name, Font Style (eg. 'B' for Bold), Font Size
$pdf->SetTextColor(0,0,0);
$pdf->Cell(200,20,"VISITORS LIST ($date)","0","1","C");
//$pdf->setLeftMargin(20);
$pdf->Cell(20,10,"Token No","1","0","C");
$pdf->Cell(40,10,"Name","1","0","C");
$pdf->Cell(40,10,"Locality","1","0","C");
$pdf->Cell(40,10,"Phone No","1","0","C");
$pdf->Cell(20,10,"Section ID","1","0","C");
$pdf->Cell(30,10,"Visit Purpose","1","0","C");
//$pdf->setFont("Arial","",14);

if(isset($_GET['date']))
{
    $date=$_GET['date'];
    $result = $conn->query("SELECT * FROM pass WHERE date_of_pass LIKE '$date%' AND taluk_id=$taluk_id");
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $pdf->Ln();
            $pdf->Cell(20,10,$row['token_number'],"1","0","C");
            $pdf->Cell(40,10,$row['name'],"1","0","C");
            $pdf->Cell(40,10,$row['locality'],"1","0","C");
            $pdf->Cell(40,10,$row['phone_number'],"1","0","C");
            $pdf->Cell(20,10,$row['section_id'],"1","0","C");
            $pdf->Cell(30,10,$row['visit_purpose'],"1","0","C");
        }
    }
}

$pdf->Output('D','VISITORS LIST.pdf');
?>

	
