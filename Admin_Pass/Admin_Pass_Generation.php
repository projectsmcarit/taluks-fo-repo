<?php 
session_start();
if(!isset($_SESSION['taluk_id']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php" ;
	$taluk_id=$_SESSION['taluk_id'];
	$result=$conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id"); 
	$row=$result->fetch_assoc();
	$taluk=$row['taluk_name'];
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../form.css">
</head>
<body>
	<?php
	include "../header.php";
	?>
	
	<div class="wrapper">
      <div class="text-center mt-4 name">GENERATE PASS</div>
      <form class="p-3 mt-3" method="post">
		<label>
				<input type="text" id="name" name="name" placeholder="Name *" required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
		</label>
		<label>
			<input type="text" id="locality" placeholder="Locality" name="locality" pattern="[A-Za-z ]+" value="">
		</label>  
		<label>
		    <input type="tel" id="phone_number" name="phone_number" placeholder="Phone Number" pattern="[0-9]{10}" title="Only 10 digits are allowed" value="">
		</label>
		<label>
		        		<?php
                            $sql="select section_id, section_name from section where for_pass='Y' and taluk_id=$taluk_id";
                            $result=$conn->query($sql);
                        ?>    
                            <select id="section" name="section" placeholdere="Section">
                            	<option value="" selected>Select</option>
                        <?php    	
                            if($result->num_rows>0)
	                            while($row=$result->fetch_assoc())
		                            echo '<option value="'.$row['section_id'].'">'.$row['section_name'].'</option>';
                            echo '</select>';
                        ?>  
						</label>  
		        	<label>
		        		<input type="text" id="purpose" name="purpose" value="" placeholder="Visit Purpose">
					</label>
	        	<div class="btns">
					<button type="reset" class="button"><i class="material-icons">restart_alt</i>Reset</button>
					<button class="blue button" type="submit" name="submit"><i class="material-icons">done</i>Submit</button>
         		</div>
        	</form>
    </div>
    
    <?php   
        if (isset($_POST['submit'])) 
        {        	
        	$name=$_POST['name'];
        	$locality=$_POST['locality'];
        	$phone_number=$_POST['phone_number'];
        	$section=$_POST['section'];
        	$purpose=$_POST['purpose'];
        	date_default_timezone_set("Asia/Kolkata");
        	$date=date("Y-m-d h:i:s",time());
        	$sql1="SELECT token_number, date_of_pass FROM pass where taluk_id=$taluk_id ORDER BY pass_id DESC LIMIT 1";     
        	$result1=$conn->query($sql1);       		
        	if($row1=$result1->fetch_assoc())
        	{
        		if(date("Y-m-d",strtotime($row1['date_of_pass']))==date("Y-m-d",strtotime($date)))
        		    $token_number=$row1['token_number']+1;
        		else
        			$token_number=1;
        	}
        	if ($section=="") 
        	    $sql2="INSERT INTO pass (token_number, name, locality, phone_number, visit_purpose, date_of_pass,taluk_id) VALUES ($token_number,'$name','$locality','$phone_number','$purpose','$date','$taluk_id');";   	    	
        	else
        	    $sql2="INSERT INTO pass (token_number, name, locality, phone_number, section_id, visit_purpose, date_of_pass,taluk_id) VALUES ($token_number,'$name','$locality','$phone_number',$section,'$purpose','$date','$taluk_id')";      	           	
        	if($conn->query($sql2))
	        { 	            	
	?>
		        <script type="text/javascript"> 
					alert("Successful");
                    window.onload=function() 
                    {
                        window.open('Pass_Receipt.php');
                    }
                </script>   
    <?php	  
            } 
            else
            {
	?>
                <script type="text/javascript"> 
					alert("Failed");
                    location.replace("Admin_Pass_Generation.php");
                </script>
    <?php
            }
        }
		//including footer file
		include "../Footer.php";
	?>
</body>
</html>