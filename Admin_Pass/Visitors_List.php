<?php
session_start();
if (!isset($_SESSION['taluk_id'])) {
    echo "<script>alert('Session Expired');</script>";
    echo '<script type="text/javascript">location.replace("../index.php");</script>';
}
include "../connection.php";
$taluk_id = $_SESSION['taluk_id'];
$result = $conn->query("SELECT taluk_name From taluk where taluk_id=$taluk_id");
$row = $result->fetch_assoc();
$taluk = $row['taluk_name'];
include "../header.php";
include "../Footer.php";
date_default_timezone_set("Asia/Kolkata");
?>
<!DOCTYPE html>
<html>

<head>
    <title>Visitors List</title>
    <link rel="stylesheet" href="../form.css">
    <style type="text/css">
        .wrapper {
            max-width: 95%;
            min-height: 250px;
            margin: 80px auto;
            padding: 40px 30px 30px 30px;
            background-color: #ecf0f3;
            border-radius: 15px;
            box-shadow: 13px 13px 20px #cbced1, -13px -13px 20px #fff;
        }

        .material-icons {
            vertical-align: -15%;
            font-size: 18px;
        }

        .date {
            margin: 0;
        }

        input[type=date] {
            width: 95%;
        }

        .btns {
            padding: 0 250px;
        }

        #search {
            height: 59.41px;
            width: 150px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="heading">LIST OF VISITORS</div>
        <form method="POST">
            <div class="btns">
                <label class="date">
                    <input type="date" name="date" id="date" value="<?php echo date('Y-m-d', time()); ?>" />
                </label>
                <button type="submit" name="search" id="search" class="blue button"><i class="material-icons">search</i>Search</button>              
            </div>
        </form>

        <div id="list" style="width: 100%">
            <?php
            $date = date("Y-m-d", time());
            $result = $conn->query("SELECT * FROM pass WHERE date_of_pass LIKE '$date%' AND taluk_id=$taluk_id");
            if ($result->num_rows > 0) {
            ?>
                <table class="table">
                    <tr>
                        <th>Token Number</th>
                        <th>Name</th>
                        <th>Locality</th>
                        <th>Phone Number</th>
                        <th>Section ID</th>
                        <th>Visit Purpose</th>                    
                    </tr>
                    
                    <?php
                    while ($row = $result->fetch_assoc()) {
                        $token_no = $row['token_number'];
                        $name = $row['name'];
                        $locality = $row['locality'];
                        $phn_no = $row['phone_number'];
                        $section_id = $row['section_id'];
                        $visit_purpose = $row['visit_purpose'];
                    ?>
                        <tr>
                            <td><?php echo $token_no; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $locality; ?></td>
                            <td><?php echo $phn_no; ?></td>
                            <td><?php echo $section_id; ?></td>
                            <td><?php echo $visit_purpose; ?></td>
                            
                        </tr>
                    <?php
                    }
                    ?>
                </table>
                <div style="text-align: center;">
                    <a href="Visitors_Pdf.php?date=<?php echo  date('Y-m-d', time()); ?>"><button class="button blue" type="button"><i class="material-icons">print</i>Print</button></a>
                </div>
            <?php
            } else {
                echo "<div class='text'>Nothing to display</div>";
            }
            ?>
        </div>

    </div>

    <br>
</body>

</html>

<?php
if (isset($_REQUEST['search'])) {
    $date = date('Y-m-d', strtotime($_REQUEST['date']));

?>
   <script>
        document.getElementById('date').value = "<?php echo $date ?>";
    </script>
    <?php
    $result = $conn->query("SELECT * FROM pass WHERE date_of_pass LIKE '$date%' AND taluk_id=$taluk_id;");

    if ($result->num_rows > 0) {
    ?>
        <script type="text/javascript">
            var content = '<table class="table"><tr><th>Token Number</th><th>Name</th><th>Locality</th><th>Phone Number</th><th>Section ID</th><th>Visit Purpose</th> </tr>';
        </script>
        <?php
        while ($row = $result->fetch_assoc()) {

            $token_no = $row['token_number'];
            $name = $row['name'];
            $locality = $row['locality'];
            $phn_no = $row['phone_number'];
            $section_id = $row['section_id'];
            $visit_purpose = $row['visit_purpose'];
        ?>
            <script type="text/javascript">
                content += '<tr><td><?php echo $token_no; ?></td><td><?php echo $name; ?></td><td><?php echo $locality; ?></td><td><?php echo $phn_no; ?></td><td><?php echo $section_id; ?></td><td><?php echo $visit_purpose; ?></td></tr>';
            </script>
        <?php
        }
        ?>
        <script>
            content += '</table><div style="text-align: center;"><a href="Visitors_Pdf.php?date=<?php echo  $date; ?>"><button class="button blue" type="button"><i class="material-icons">print</i>Print</button></a></div>';
        </script>
        <?php
        
    } else {
        ?>
        <script>
            content = "<div class='text'>Nothing to display</div>";
        </script>
    <?php
    }
    ?>
    <script type="text/javascript">
        document.getElementById('list').innerHTML = content;
    </script>
<?php
}

?>