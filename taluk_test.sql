-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2022 at 03:19 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE USER IF NOT EXISTS 'root'@'localhost' IDENTIFIED BY '';

CREATE DATABASE IF NOT EXISTS taluk_test;

GRANT ALL PRIVILEGES ON taluk_test . * TO 'root'@'localhost';

USE taluk_test;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taluk_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE `application` (
  `application_number` varchar(25) NOT NULL,
  `file_number` varchar(20) DEFAULT NULL,
  `person_id` varchar(15) NOT NULL,
  `section_submitted` int(11) NOT NULL,
  `application_subject` varchar(50) NOT NULL,
  `date_applied` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `section_current` int(11) NOT NULL,
  `list_of_sections` varchar(50) DEFAULT NULL,
  `taluk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`application_number`, `file_number`, `person_id`, `section_submitted`, `application_subject`, `date_applied`, `status`, `section_current`, `list_of_sections`, `taluk_id`) VALUES
('20001', NULL, '101', 45, 'Dummy Value', '2021-01-01 00:00:00', 'fresh', 45, 'NULL', 1),
('20002', NULL, '106', 24, 'na', '2021-01-01 05:07:29', 'Fresh', 24, '', 1),
('20003', NULL, '107', 16, 'certificate', '2021-01-01 05:14:52', 'Fresh', 16, '', 1),
('20004', NULL, '108', 21, 'CERT FOR UNMARRIED NATIVITY CAST', '2021-01-01 05:27:55', 'Fresh', 21, '', 1),
('20005', NULL, '109', 21, 'certificate FOR ews', '2021-01-01 05:38:47', 'Fresh', 21, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `enquiry_number` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `village` varchar(25) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `application_number` varchar(25) DEFAULT NULL,
  `file_number` varchar(25) DEFAULT NULL,
  `date_application` datetime DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `date_enquiry` datetime NOT NULL,
  `taluk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `taluk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `taluk_id`) VALUES
('admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pass`
--

CREATE TABLE `pass` (
  `pass_id` int(11) NOT NULL,
  `token_number` int(25) NOT NULL,
  `name` varchar(30) NOT NULL,
  `locality` varchar(30) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `visit_purpose` varchar(50) DEFAULT NULL,
  `date_of_pass` datetime NOT NULL,
  `taluk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pass`
--

INSERT INTO `pass` (`pass_id`, `token_number`, `name`, `locality`, `phone_number`, `section_id`, `visit_purpose`, `date_of_pass`, `taluk_id`) VALUES
(1, 0, '', '', '', 1, 'na', '2021-01-01 05:06:59', 1),
(2, 0, '', '', '', 25, 'flood', '2021-01-01 06:13:15', 1),
(3, 0, '', '', '', 19, 'land revenue', '2021-01-02 10:21:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `person_id` varchar(15) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `address3` varchar(30) DEFAULT NULL,
  `post_office` varchar(20) DEFAULT NULL,
  `pin_code` int(6) DEFAULT NULL,
  `village` varchar(20) DEFAULT NULL,
  `mobile_number` varchar(10) DEFAULT NULL,
  `email_id` varchar(30) DEFAULT NULL,
  `land_number` varchar(10) DEFAULT NULL,
  `taluk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `name`, `address1`, `address2`, `address3`, `post_office`, `pin_code`, `village`, `mobile_number`, `email_id`, `land_number`, `taluk_id`) VALUES
('101', 'Dummy Value', 'Dummy Value', 'Dummy Value', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
('102', 'sreekumar', 'dd', 'dd', '', '', 0, '', '', '', '', 1),
('103', 'jijth', 'jiyth', 'kottayam', '', '', 0, '', '', '', '', 1),
('104', 'sheril', 'thottakuthu', 'kottayam', '', '', 0, '', '', '', '', 1),
('105', 'sreekumar', 'kumar', 'kottayam', '', '', 0, '', '', '', '', 1),
('106', 'sreekumar', 'lalajith', 'kottayam', '', '', 0, '', '', '', '', 1),
('107', 'muneer', 'thaikudam', 'kottayam', '', '', 0, '', '', '', '', 1),
('108', 'ATHUL ROBY', 'VALIYAMADA', 'AYMANAM', '', '', 0, '', '', '', '', 1),
('109', 'ROHITH PRASAD', 'MATTATHIL', 'KOTTAYAM', '', '', 0, '', '', '', '', 1),
('110', 'nimesh', 'karthika', 'athirampuzjha', '', '', 0, 'ATHIRAMPUZHA', '', '', '', 1),
('111', 'Tomsy Paul', 'gjhjhgj', 'hgjhgjg', 'kjhkhk', 'mbmnb', 0, '', '', '', '', 1),
('112', 'nixon', '23', 'ktm', 'pampady', '682597', 0, 'PAMPADY', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(50) NOT NULL,
  `for_application` char(1) NOT NULL,
  `for_pass` char(1) NOT NULL,
  `section_purpose` varchar(100) NOT NULL,
  `taluk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`section_id`, `section_name`, `for_application`, `for_pass`, `section_purpose`, `taluk_id`) VALUES
(1, 'Tahsildar', 'N', 'Y', '', 1),
(2, 'Tahsildar (LR)', 'N', 'Y', '', 1),
(3, 'Deputy Tahsildar (HQ)', 'N', 'N', '', 1),
(4, 'Deputy Tahsildar Election', 'N', 'N', '', 1),
(5, 'Head Surveyor', 'N', 'N', '', 1),
(6, 'Surveyor_________________', 'N', 'N', '', 1),
(7, 'Deputy Tahsildar - A', 'N', 'N', '', 1),
(8, 'Deputy Tahsildar - B', 'N', 'N', '', 1),
(9, 'Deputy Tahsildar - C', 'N', 'N', '', 1),
(10, 'Deputy Tahsildar - D', 'N', 'N', '', 1),
(11, 'Deputy Tahsildar - E', 'N', 'N', '', 1),
(12, 'Deputy Tahsildar - F', 'N', 'N', '', 1),
(13, 'Deputy Tahsildar - G', 'N', 'N', '', 1),
(14, 'Deputy Tahsildar - H', 'N', 'N', '', 1),
(15, 'A1', 'Y', 'N', 'Establishment', 1),
(16, 'A2', 'Y', 'N', 'Compassionate employment', 1),
(17, 'A3', 'Y', 'N', '', 1),
(18, 'A4', 'Y', '', '', 1),
(19, 'A5', 'Y', '', '', 1),
(20, 'A6', 'Y', '', '', 1),
(21, 'B1', 'Y', 'N', 'Certificate', 1),
(22, 'B2', 'Y', 'N', 'Arms and Explosive/mining, Electricity, Covid-19, Law and order', 1),
(23, 'B3', 'Y', 'N', 'Tree cutting, Birth/death registration, Miscellaneous', 1),
(24, 'C1', 'Y', 'N', 'Land transfer', 1),
(25, 'C2', 'Y', 'N', 'Disaster management', 1),
(26, 'C3', 'Y', 'N', 'CMDRF', 1),
(27, 'C4', 'Y', 'N', 'NFBS', 1),
(28, 'D1', 'Y', 'N', 'Luxury tax', 1),
(29, 'D2', 'Y', '', '', 1),
(30, 'D3', 'Y', '', '', 1),
(31, 'D4', 'Y', '', '', 1),
(32, 'D5', 'Y', '', '', 1),
(33, 'F1', 'Y', 'N', 'Encroachment of land,Etiction', 1),
(34, 'F2', 'Y', 'N', 'Land assignment, Pattayam', 1),
(35, 'F3', 'Y', 'N', 'Surplus land', 1),
(36, 'F4', 'Y', '', '', 1),
(37, 'G1', 'Y', 'N', 'Resurvey correction, PV, Point out of boundary, Area alteration', 1),
(38, 'G2', 'Y', '', '', 1),
(39, 'G3', 'Y', '', '', 1),
(40, 'G4', 'Y', '', '', 1),
(41, 'H1', 'Y', '', '', 1),
(42, 'J1', 'Y', 'N', 'Election', 1),
(43, 'K1', 'Y', 'N', 'CMO files', 1),
(44, 'K2', 'Y', 'N', 'Revenue recovery, Adalath', 1),
(45, 'K3', 'Y', 'N', 'KLU(Land utilization),Paddy and wetland act, Land conversion', 1),
(50, 'E2', 'Y', 'N', 'Pensions', 1),
(51, 'E3', 'Y', 'N', 'P V cancellation', 1),
(52, 'E4', 'Y', 'N', 'Legal heirship certificate', 1),
(53, 'E5', 'Y', 'N', '', 1),
(54, 'E1', 'Y', 'N', '', 1),
(55, 'B4', 'Y', 'N', 'Pumping subsidy, RIT(Right to Information Act), Right to service, Auction', 1),
(56, 'B5', 'Y', 'N', 'Irrigation', 1);

-- --------------------------------------------------------

--
-- Table structure for table `taluk`
--

CREATE TABLE `taluk` (
  `taluk_id` int(11) NOT NULL,
  `taluk_name` varchar(30) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phn_no` varchar(10) DEFAULT NULL,
  `email_id` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `taluk`
--

INSERT INTO `taluk` (`taluk_id`, `taluk_name`, `address`, `phn_no`, `email_id`) VALUES
(1, 'KOTTAYAM', 'Kottayam', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`application_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `section_current` (`section_current`),
  ADD KEY `section_submitted` (`section_submitted`),
  ADD KEY `taluk_id` (`taluk_id`) USING BTREE;

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`enquiry_number`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `taluk_id` (`taluk_id`) USING BTREE;

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`username`),
  ADD KEY `taluk_id` (`taluk_id`);

--
-- Indexes for table `pass`
--
ALTER TABLE `pass`
  ADD PRIMARY KEY (`pass_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `taluk_id` (`taluk_id`) USING BTREE;

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `taluk_id` (`taluk_id`) USING BTREE;

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `taluk_id` (`taluk_id`) USING BTREE;

--
-- Indexes for table `taluk`
--
ALTER TABLE `taluk`
  ADD PRIMARY KEY (`taluk_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `enquiry_number` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pass`
--
ALTER TABLE `pass`
  MODIFY `pass_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `taluk`
--
ALTER TABLE `taluk`
  MODIFY `taluk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `application`
--
ALTER TABLE `application`
  ADD CONSTRAINT `application_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`),
  ADD CONSTRAINT `application_ibfk_2` FOREIGN KEY (`section_current`) REFERENCES `section` (`section_id`),
  ADD CONSTRAINT `application_ibfk_3` FOREIGN KEY (`section_submitted`) REFERENCES `section` (`section_id`),
  ADD CONSTRAINT `application_ibfk_4` FOREIGN KEY (`taluk_id`) REFERENCES `taluk` (`taluk_id`);

--
-- Constraints for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD CONSTRAINT `enquiry_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`),
  ADD CONSTRAINT `enquiry_ibfk_2` FOREIGN KEY (`taluk_id`) REFERENCES `taluk` (`taluk_id`);

--
-- Constraints for table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`taluk_id`) REFERENCES `taluk` (`taluk_id`);

--
-- Constraints for table `pass`
--
ALTER TABLE `pass`
  ADD CONSTRAINT `pass_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`),
  ADD CONSTRAINT `pass_ibfk_2` FOREIGN KEY (`taluk_id`) REFERENCES `taluk` (`taluk_id`);

--
-- Constraints for table `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `person_ibfk_1` FOREIGN KEY (`taluk_id`) REFERENCES `taluk` (`taluk_id`);

--
-- Constraints for table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`taluk_id`) REFERENCES `taluk` (`taluk_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
